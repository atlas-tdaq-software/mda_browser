#ifndef MDA_BROWSER_LBTOOLMODEL_H
#define MDA_BROWSER_LBTOOLMODEL_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include <QAbstractItemModel>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda_browser/LbToolEntry.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  Model that represents an ordered set of LbToolEntry objects.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LbToolModel : public QAbstractItemModel {

    Q_OBJECT

public:

    /// column indices
    enum Columns { RunColumn, LbColumn, FactorColumn,  OpColumn, PrevFactorColumn };

    // Default constructor
    LbToolModel (QObject* parent = 0);
    LbToolModel (QObject* parent, const std::vector<LbToolEntry>& initial);
    
    // Destructor
    virtual ~LbToolModel () ;

    // Returns the number of columns for the children of the given parent.
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    // Returns the data stored under the given role for the item referred to by the index.
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

    // Returns the item flags for the given index.
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;

    // Returns the data for the given role and section in the header with the specified orientation.
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    // Returns the index of the item in the model specified by the given row, column and parent index.
    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

    // inserts count rows into the model before the given row
    virtual bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex());

    // Returns the parent of the model item with the given index.
    virtual QModelIndex parent(const QModelIndex& index) const;

    // removes count rows starting with the given row under parent parent from the model
    virtual bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

    // Returns the number of rows under the given parent
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;

    // Sets the role data for the item at index to value
    virtual bool setData(const QModelIndex & index, const QVariant& value, int role = Qt::EditRole);

    // move row to a new position
    bool moveRow(int row, int newRow);

    // Get current model data, data is modifiable but the only thing that you may
    // legally change in the data is file name and histogram name, do not change
    // data that is displayed in the dialog, and do not add/delete new entries
    std::vector<LbToolEntry>& entries() { return m_data; }
    
    // Replace current data
    void replaceData(const std::vector<LbToolEntry>& entries);
    
public slots:

    // reset model to its initial state
    void resetToInitial();

    // copy all factors from given entry to all other entries
    void copyFactors(int row);

protected:

private:

    // Data members
    std::vector<LbToolEntry> m_initial;
    std::vector<LbToolEntry> m_data;
    
};

} // namespace mda_browser

#endif // MDA_BROWSER_LBTOOLMODEL_H
