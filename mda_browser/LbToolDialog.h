#ifndef MDA_BROWSER_LBTOOLDIALOG_H
#define MDA_BROWSER_LBTOOLDIALOG_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <QDialog>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ui_lb_tool_dialog.h"
#include "mda_browser/CanvasMgr.h"
#include "mda_browser/DbConnections.h"
#include "mda_browser/FileCache.h"
#include "mda_browser/LbToolModel.h"
#include <QItemSelection>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  Main LbTool window.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LbToolDialog : public QDialog, public Ui::LbToolDialog {

    Q_OBJECT

public:

    // Default constructor
    LbToolDialog (QWidget* parent,
            DbConnections* dbconn,
            FileCache* fileCache,
            const QString& partition,
            const QString& server,
            const QString& provider,
            const QString& path,
            const std::vector<LbToolEntry>& entries) ;
    
    // Destructor
    virtual ~LbToolDialog () ;

protected:

    // returns the index of the selected row, returns -1 if no selection
    int selectedRow() const;

    // Update file names and histograms paths of entries that miss this info,
    // returns true if all entries have their file names and paths OK.
    bool updateFileNames();

private slots:

    // update buttons based on the model state and selection
    void updateButtons();

    // insert row before selected or after last one if no selection
    void insertRow();

    // remove selected row
    void removeRow();

    // move selected row up
    void moveRowUp();

    // move selected row down
    void moveRowDown();

    // save data to a file, ask for file name first
    void saveData();

    // save data to a given file name
    void saveData(const QString& fname);

    // load data from a file 
    void loadData();

    // load data from a file 
    void loadRecent(QAction* action);

    // save data from a given file name
    void loadData(const QString& fname);

    // make a plot based on current data 
    void plot();

    // copy factors and op from selected row to all other rows
    void applyFactors();

private:

    void init();

    void addRecent(const QString& string);
    void removeRecent(const QString& string);
    void fillRecentMenu();
    
    DbConnections* m_dbconn;
    FileCache* m_fileCache;
    CanvasMgr* m_canvasMgr;
    LbToolModel* m_model;
    QPushButton* m_saveButton;
    QPushButton* m_loadButton;
    QPushButton* m_plotButton;
    QPushButton* m_recentButton;
    QMenu* m_recentMenu;
};

} // namespace mda_browser

#endif // MDA_BROWSER_LBTOOLDIALOG_H
