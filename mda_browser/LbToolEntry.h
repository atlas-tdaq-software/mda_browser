#ifndef MDA_BROWSER_LBTOOLENTRY_H
#define MDA_BROWSER_LBTOOLENTRY_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <stdint.h>
#include <QString>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  Class which defines single entry (row) in LB Tool.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LbToolEntry  {
public:

    enum Op { Minus = '-', Plus = '+', Multiply = '*', Divide = '/'};

    // Default constructor
    LbToolEntry ();
    LbToolEntry (uint32_t run, uint32_t lb, double f, Op op, double fprev,
            const QString& fileName=QString(),
            const QString& dataset=QString(),
            const QString& histoPath=QString());

    uint32_t run() const { return m_run; }
    uint32_t lb() const { return m_lb; }
    QString lbString() const;
    double factor() const { return m_f; }
    double prevFactor() const { return m_fprev; }
    Op op() const { return m_op; }
    QString opString() const { return QString(QChar(char(m_op))); }
    const QString& fileName() const { return m_fileName; }
    const QString& dataset() const { return m_dataset; }
    const QString& histoPath() const { return m_histoPath; }

    void setRun(uint32_t run);
    void setLb(uint32_t lb);
    bool setLb(const QString& lb) ;
    void setFactor(double f) { m_f = f; }
    void setPrevFactor(double f) { m_fprev = f; }
    void setOp(Op op) { m_op = op; }
    bool setOp(const QString& op);
    void setFileName(const QString& fileName) { m_fileName = fileName; }
    void setDataset(const QString& dataset) { m_dataset = dataset; }
    void setHistoPath(const QString& histoPath) { m_histoPath = histoPath; }

    // compare two entries, only run number and LB number
    bool operator==(const LbToolEntry& other) const {
        return m_run == other.m_run and m_lb == other.m_lb;
    }

protected:

private:

    // Data members
    uint32_t m_run;
    uint32_t m_lb;
    double m_f;
    double m_fprev;
    Op m_op;
    QString m_fileName;   ///< ROOT file name for this entry
    QString m_dataset;    ///< CoCa dataset name for this entry
    QString m_histoPath;  ///< Histogram path in the file
};

} // namespace mda_browser

#endif // MDA_BROWSER_LBTOOLENTRY_H
