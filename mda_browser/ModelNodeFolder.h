#ifndef MDA_BROWSER_MODELNODEFOLDER_H
#define MDA_BROWSER_MODELNODEFOLDER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "ModelNode.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Representation of the histogram folder in the model.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ModelNodeFolder : public ModelNode {
public:

  // Default constructor
  ModelNodeFolder (const QString& name) ;

  // Destructor
  virtual ~ModelNodeFolder () ;

  /// get relative folder name
  const QString& name() const { return m_name; }

  /// get absolute folder name, empty string for top-level folder
  virtual QString path() const;

  /// get partition name
  virtual QString partition() const;

  /// get run number
  virtual unsigned run() const;

  /// get server name
  virtual QString server() const;

  /// get provider name
  virtual QString provider() const;

  /// Returns number of columns for children
  virtual int columnCount() const;

  /// Returns column header for children
  virtual QString columnHeader(int column) const;

  /// Return data for a given column
  virtual QVariant columnValue(int column) const;

  /// Return icon for this object
  virtual const QIcon& icon() const;

  /// Load children data from database
  virtual void fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
          std::vector<boost::shared_ptr<ModelNode> >& children);

  // column count for this class
  static int classColumnCount() { return 1; }

  // column header for this class
  static QString classColumnHeader(int column);

protected:

private:

  QString m_name;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MODELNODEFOLDER_H
