#ifndef MDA_BROWSER_CONFIGDIALOG_H
#define MDA_BROWSER_CONFIGDIALOG_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <QDialog>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ui_config_dialog.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  C++ source file code template. The first sentence is a brief summary of 
 *  what the class is for. It is followed by more detailed information
 *  about how to use the class. This doc comment must immediately precede the 
 *  class definition.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ConfigDialog : public QDialog, public Ui::ConfigDialog {

    Q_OBJECT

public:

    // Default constructor
    ConfigDialog(QWidget* parent = 0);

    // Destructor
    virtual ~ConfigDialog () ;

protected:

private slots:

    // set default values for given page
    void setDefaults(int page);

    // reset page to values in config
    void reset(int page);

    // process button click
    void buttonClicked(QAbstractButton* button);

    // save all current values to config
    void save();

private:

};

} // namespace mda_browser

#endif // MDA_BROWSER_CONFIGDIALOG_H
