#ifndef MDA_BROWSER_MODELNODERUN_H
#define MDA_BROWSER_MODELNODERUN_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "mda_browser/ModelNode.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace mda_browser {
class ModelNodePartition;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Representation of the run object in the model.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ModelNodeRun : public ModelNode {
public:

  // Default constructor
  ModelNodeRun (const daq::mda::RunInfo& runInfo) ;

  // Destructor
  virtual ~ModelNodeRun () ;

  /// get partition name
  virtual QString partition() const;

  /// get run information
  const daq::mda::RunInfo& runInfo() const { return m_runInfo; }

  /// Returns number of columns for children
  virtual int columnCount() const;

  /// Returns column header for children
  virtual QString columnHeader(int column) const;

  /// Return data for a given column
  virtual QVariant columnValue(int column) const;

  /// Return icon for this object
  virtual const QIcon& icon() const;

  /// Load children data from database
  virtual void fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
          std::vector<boost::shared_ptr<ModelNode> >& children);

  // column count for this class
  static int classColumnCount() { return 3; }

  // column header for this class
  static QString classColumnHeader(int column);

protected:

private:

  daq::mda::RunInfo m_runInfo;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MODELNODERUN_H
