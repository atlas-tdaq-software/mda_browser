#ifndef MDA_BROWSER_MODELNODEPROVIDER_H
#define MDA_BROWSER_MODELNODEPROVIDER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "mda_browser/ModelNodeFolder.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace mda_browser {
class ModelNodeRun;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Representation of the provider (plus server) object in the model.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ModelNodeProvider : public ModelNodeFolder {
public:

  // Default constructor
  ModelNodeProvider (const QString& provider, const QString& server) ;

  // Destructor
  virtual ~ModelNodeProvider () ;

  /// get partition name
  virtual QString partition() const;

  /// get run number
  virtual unsigned run() const;

  /// get server name
  virtual QString server() const;

  /// get provider name
  virtual QString provider() const;

  /// Returns number of columns for children
  virtual int columnCount() const;

  /// Returns column header for children
  virtual QString columnHeader(int column) const;

  /// Return data for a given column
  virtual QVariant columnValue(int column) const;

  /// Return icon for this object
  virtual const QIcon& icon() const;

  // column count for this class
  static int classColumnCount() { return 2; }

  // column header for this class
  static QString classColumnHeader(int column);

protected:

private:

  QString m_provider;
  QString m_server;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MODELNODEPROVIDER_H
