#ifndef MDA_BROWSER_VIEWPROXYMODEL_H
#define MDA_BROWSER_VIEWPROXYMODEL_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include <QAbstractProxyModel>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Implementation of proxy model for tree view.
 *
 *  This implementation supports sorting of the rows in the list view.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ViewProxyModel : public QAbstractProxyModel {

    Q_OBJECT

public:

  // Default constructor
  ViewProxyModel (QObject* parent = 0) ;

  // Destructor
  virtual ~ViewProxyModel () ;

  // Returns the number of columns for the children of the given parent
  virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;

  // Returns the index of the item in the model specified by the given row, column and parent index.
  virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

  // Returns the parent of the model item with the given index
  virtual QModelIndex parent(const QModelIndex& index) const;

  // Returns the number of rows under the given parent
  virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;

  void setSortRole(int role) { m_sort_role = role; }
  
  // Sets the given sourceModel to be processed by the proxy model
  virtual void setSourceModel(QAbstractItemModel* sourceModel); 
  
  // attaches current model to a different root in parent node
  void setSourceRoot(const QModelIndex& source_index);

  // Sorts the model by column in the given order
  virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);
  
  // return the model index in the proxy model that corresponds to the sourceIndex from the source model
  virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;

  // return the model index in the source model that corresponds to the proxyIndex in the proxy model
  virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

  // get sort column index
  int sortColumn() const { return m_sort_column; }

  // get sort column index
  Qt::SortOrder sortOrder() const { return m_sort_order; }

protected:

  void populateMap();

private:

  std::vector<int> m_s2p;     // maps source row index to proxy row
  std::vector<int> m_p2s;     // maps proxy row index to source row
  QModelIndex m_source_root;
  int m_column_count;
  int m_row_count;
  int m_sort_column;
  Qt::SortOrder m_sort_order;
  int m_sort_role;

};

} // namespace mda_browser

#endif // MDA_BROWSER_VIEWPROXYMODEL_H
