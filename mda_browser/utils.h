#ifndef MDA_BROWSER_UTILS_H
#define MDA_BROWSER_UTILS_H
//--------------------------------------------------------------------------
// File Information:
//      Small set of utility methods usable by other classes
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <QModelIndex>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/**
 * Stream insertion operator for QModelIndex class
 */
inline
std::ostream&
operator<<(std::ostream& out, const QModelIndex& index) {
    if (index.isValid()) {
        out << "QModelIndex(row=" << index.row()
                << ", column=" << index.column();
        auto ptr = index.internalPointer();
        if (ptr != nullptr) {
            out << ", ptr=" << ptr;
        }
        auto parent = index.parent();
        if (parent.isValid()) {
            out << ", parent=" << index.parent();
        }
        out << ")";
    } else {
        out << "QModelIndex()";
    }
    return out;
}

} // namespace mda_browser

#endif // MDA_BROWSER_UTILS_H
