#ifndef MDA_BROWSER_DBCONNECTIONS_H
#define MDA_BROWSER_DBCONNECTIONS_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/shared_ptr.hpp>

//----------------------
// Base Class Headers --
//----------------------
#include <QObject>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  Class which manages database connections.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DbConnections : public QObject {

    Q_OBJECT

public:

    // Default constructor
    DbConnections(QObject* parent) ;
    
    // Destructor
    virtual ~DbConnections () ;
    
    // Returns MDA client object. If connection cannot be established
    // it will show warning message box and return zero pointer.
    boost::shared_ptr<daq::mda::DBRead> getMda();
    
    // Returns file reader client. If connection cannot be established
    // it will show warning message box and return zero pointer.
    boost::shared_ptr<daq::mda::FileRead> getFileRead();

public slots:

    // reset connections if config changed
    void reset();
    
protected:

private:
    
    boost::shared_ptr<daq::mda::DBRead> m_mdaDb;
    boost::shared_ptr<daq::mda::FileRead> m_mdaFileRead;

};

} // namespace mda_browser

#endif // MDA_BROWSER_DBCONNECTIONS_H
