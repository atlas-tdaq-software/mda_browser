#ifndef MDA_BROWSER_CANVASWINDOW_H
#define MDA_BROWSER_CANVASWINDOW_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <TObject.h>

//----------------------
// Base Class Headers --
//----------------------
#include <QMainWindow>
#include "ui_canvas_window.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class TCanvas;

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @breief Window which displays histograms in ROOT canvas
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class CanvasWindow : public QMainWindow, public Ui::CanvasWindow {

    Q_OBJECT

public:

    // Default constructor
    CanvasWindow (QWidget* parent = 0) ;
    
    // Destructor
    virtual ~CanvasWindow () ;
    
protected:

    // override closeEvent to store settings 
    virtual void closeEvent(QCloseEvent* event); 

public slots:
  
    // Delete all contents from a canvas
    void clear();
    
    // Add one more object and show it in a new pad
    void add(TObject* obj);

    /// save window geometry
    void saveGeometry();

    /// restore window geometry
    void restoreGeometry();

    /// save canvas contents
    void saveCanvas();

private:

    TCanvas* m_canvas;

};

} // namespace mda_browser

#endif // MDA_BROWSER_CANVASWINDOW_H
