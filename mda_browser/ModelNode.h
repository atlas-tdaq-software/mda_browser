#ifndef MDA_BROWSER_MODELNODE_H
#define MDA_BROWSER_MODELNODE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>
#include <vector>
#include <QVariant>
#include <QIcon>
#include <QModelIndex>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Base class for the model tree nodes.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ModelNode : boost::noncopyable {
public:

  // Destructor
  virtual ~ModelNode () ;

  /// Get parent node or 0
  ModelNode* parent() const { return m_parent; }

  /// Set parent node
  void setParent(ModelNode* parent) { m_parent = parent; }
  
  /// returns true if node is a leaf node
  virtual bool leaf() const;

  /// Get node index (row) in its parent
  int row() const { return m_row; }

  /// Set node index (row) in its parent
  void setRow(int row) { m_row = row; }
  
  /// Get child node at given index (may return 0)
  ModelNode* child(int index) const {
      if (index < 0 or index >= int(m_children.size())) return 0;
      return m_children[index].get();
  }
  
  /// Returns true when node has children (or if it's not populated yet)
  bool hasChildren() const { return not m_populated or not m_children.empty(); }
  
  /// Returns number of children
  int rowCount() const { return m_children.size(); }

  /// Returns number of columns for children
  virtual int columnCount() const = 0;

  /// Returns column header for children
  virtual QString columnHeader(int column) const = 0;

  /// Return data for a given column
  virtual QVariant columnValue(int column) const = 0;

  /// Return data for a given column used for sorting,
  /// default is to return columnValue()
  virtual QVariant sortValue(int column) const;

  /// Return icon for this object
  virtual const QIcon& icon() const = 0;
  
  /// Return true if the object has been populated with children
  bool populated() const { return m_populated; }

  /// Load children data from database
  virtual void fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
          std::vector<boost::shared_ptr<ModelNode> >& children) = 0;

  /// Add children nodes, call only if populated() returns false
  virtual void populate(const std::vector<boost::shared_ptr<ModelNode> >& children);

  // remove all children, reset populated flag
  virtual void clearChildren();

protected:

  // Default constructor
  ModelNode (ModelNode* parent = 0, int row = 0) ;

  // set populated flag
  void setPopulated(bool flag) { m_populated = flag; }

private:

  ModelNode* m_parent;
  int m_row;
  bool m_populated;
  std::vector<boost::shared_ptr<ModelNode> > m_children;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MODELNODE_H
