#ifndef MDA_BROWSER_CANVASMGR_H
#define MDA_BROWSER_CANVASMGR_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <set>

//----------------------
// Base Class Headers --
//----------------------
#include <QObject>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <QWidget>
#include "mda_browser/CanvasWindow.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Class which manages all canvas windows
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class CanvasMgr : public QObject {
    
    Q_OBJECT
    
public:

    // Default constructor
    explicit CanvasMgr (QWidget* parent) ;
    
    // Destructor
    virtual ~CanvasMgr ();

    // make new canvas window
    CanvasWindow* newCanvas();
    
    // get last or make new canvas window
    CanvasWindow* lastCanvas();
    
private:

    QWidget* m_parent;
    
};

} // namespace mda_browser

#endif // MDA_BROWSER_CANVASMGR_H
