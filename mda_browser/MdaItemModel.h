#ifndef MDA_BROWSER_MDAITEMMODEL_H
#define MDA_BROWSER_MDAITEMMODEL_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <QAbstractItemModel>
#include <QStringList>
#include <boost/shared_ptr.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"
#include "mda_browser/DbConnections.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace mda_browser {
class ModelNode;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Qt data model for MDA.
 *  
 *  Implementation of the QAbstractItemModel for MDA data.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class MdaItemModel : public QAbstractItemModel {
    
    Q_OBJECT
    
public:

    // Default constructor
    MdaItemModel(DbConnections* dbconn, QObject* parent = 0);
    
    // Destructor
    virtual ~MdaItemModel () ;

    /// Returns true if node at given index has children
    virtual bool hasChildren(const QModelIndex& parent) const;

    /// Returns true for items that may need to bring data from database
    virtual bool canFetchMore(const QModelIndex& parent) const;

    /// get data from database or refresh current data
    virtual void fetchMore(const QModelIndex& parent);

    /// get number of children for given node
    virtual int rowCount(const QModelIndex& parent) const;

    /// get number of columns for the children of this node
    virtual int columnCount(const QModelIndex& parent) const;

    /// get item data
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;

    /// get header data
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    /// get the index for the child of a given node
    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

    /// get the index for a given node
    virtual QModelIndex index(ModelNode* node, int column = 0) const;

    /// get the index of the parent of this node
    virtual QModelIndex parent(const QModelIndex& index) const;

public slots:

    /// remove all children of the given parent
    void deleteChildren(const QModelIndex& index);

    /// remove all grand children of the given node
    void deleteGrandChildren(const QModelIndex& index);

    /// populate given parent with info from database
    void populate(const QModelIndex& index);

    /// set model headers from given item
    void setHeaders(const QModelIndex& index);

private:

    DbConnections* m_dbconn;
    boost::shared_ptr<ModelNode> m_rootNode;
    QStringList m_headers;
    
    // Copy constructor and assignment are disabled by default
    MdaItemModel ( const MdaItemModel& ) ;
    MdaItemModel& operator = ( const MdaItemModel& ) ;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MDAITEMMODEL_H
