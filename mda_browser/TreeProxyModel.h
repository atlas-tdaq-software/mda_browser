#ifndef MDA_BROWSER_TREEPROXYMODEL_H
#define MDA_BROWSER_TREEPROXYMODEL_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <QSortFilterProxyModel>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Implementation of proxy model for tree view.
 *
 *  This model does filtering so that tree view doe not show any leaf nodes.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class TreeProxyModel : public QSortFilterProxyModel {

    Q_OBJECT

public:

  // Default constructor
  TreeProxyModel(QObject* parent = 0) ;

  // Destructor
  virtual ~TreeProxyModel () ;

protected:

  /// Returns true if the item in the column indicated by the given source_column and source_parent
  /// should be included in the model
  virtual bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const;

  /// Returns true if the item in the row indicated by the given source_row and source_parent
  /// should be included in the model
  virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;

private:

};

} // namespace mda_browser

#endif // MDA_BROWSER_TREEPROXYMODEL_H
