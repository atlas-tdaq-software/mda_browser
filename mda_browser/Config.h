#ifndef MDA_BROWSER_CONFIG_H
#define MDA_BROWSER_CONFIG_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <QSettings>

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  Class which is a wrapper for QSettings class which also knows about
 *  correct default values. It is a light-weight class (just as
 *  QSetings) and can be constructed and destroyed at wish.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Config  {
public:

    // Default constructor
    Config () ;

    // Destructor
    ~Config () ;

    // Returns the value for setting key. If the setting doesn't exist, returns defaultValue.
    // If defaultValue is not specified returns application default.
    QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const;

    // Sets the value of setting key to value. If the key already exists, the previous value is overwritten.
    void setValue(const QString& key, const QVariant& value);

    // Returns application default value for setting key.
    QVariant defaultValue(const QString& key) const;

protected:

private:

    // Data members
    mutable QSettings m_config;

};

} // namespace mda_browser

#endif // MDA_BROWSER_CONFIG_H
