#ifndef MDA_BROWSER_FILECACHE_H
#define MDA_BROWSER_FILECACHE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <list>
#include <map>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <TFile.h>
#include "mda/mda.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Cache for TFile objects used by mda_browser.
 *  
 *  This class caches most recently used TFile objects so that the can be reused
 *  when different histogram from the same file is accessed.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class FileCache : boost::noncopyable {
public:

    // Default constructor
    FileCache(unsigned maxSize = 32);
    
    // Destructor
    ~FileCache () ;

    /// Return cached or newly open file for given file name and dataset.
    boost::shared_ptr<TFile> getFile(const std::string& filename, const std::string& dataset,
            const boost::shared_ptr<daq::mda::FileRead>& mdaFileRead);
    
private:

    typedef std::pair<std::string, std::string> DSFileName; // Combination of dataset and file name
    typedef std::map<DSFileName, boost::shared_ptr<TFile> > CacheMap;
    
    unsigned m_maxSize;           ///< Cache size
    CacheMap m_name2file;         ///< Maps file name to file object
    std::list<DSFileName> m_mru;  ///< Most recently used names
  
};

} // namespace mda_browser

#endif // MDA_BROWSER_FILECACHE_H
