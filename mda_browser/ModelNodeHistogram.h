#ifndef MDA_BROWSER_MODELNODEHISTOGRAM_H
#define MDA_BROWSER_MODELNODEHISTOGRAM_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "ModelNode.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace mda_browser {
class ModelNodeFolder;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Representation of the histogram object in the model.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class ModelNodeHistogram : public ModelNode {
public:

  // Default constructor
  ModelNodeHistogram (const QString& name,
          const std::set<daq::mda::HistoFile>& histos) ;

  // Destructor
  virtual ~ModelNodeHistogram () ;

  /// get relative histogram name
  const QString& name() const { return m_name; }

  /// get absolute histogram name
  virtual QString path() const;

  /// returns true if node is a leaf node
  virtual bool leaf() const;

  /// Returns number of columns for children
  virtual int columnCount() const;

  /// Returns column header for children
  virtual QString columnHeader(int column) const;

  /// Return data for a given column
  virtual QVariant columnValue(int column) const;

  /// Return data for a given column used for sorting
  virtual QVariant sortValue(int column) const;

  /// Return icon for this object
  virtual const QIcon& icon() const;

  /// Load children data from database
  virtual void fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
          std::vector<boost::shared_ptr<ModelNode> >& children);

  /// Get histogram file object
  const daq::mda::HistoFile& hfile() const { return m_histo; }

  // column count for this class
  static int classColumnCount() { return 5; }

  // column header for this class
  static QString classColumnHeader(int column);

protected:

private:

  QString m_name;
  daq::mda::HistoFile m_histo;
  QString m_lbString;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MODELNODEHISTOGRAM_H
