#ifndef MDA_BROWSER_MDABROWSER_H
#define MDA_BROWSER_MDABROWSER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <QAbstractProxyModel>
#include <QItemSelection>
#include <QLabel>
#include <boost/shared_ptr.hpp>

//----------------------
// Base Class Headers --
//----------------------
#include <QMainWindow>
#include "ui_main_window.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"
#include "mda_browser/CanvasMgr.h"
#include "mda_browser/DbConnections.h"
#include "mda_browser/FileCache.h"
#include "mda_browser/TreeProxyModel.h"
#include "mda_browser/ViewProxyModel.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace mda_browser {
class MdaItemModel;
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @brief Main window of the mda_browser application.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class MdaBrowser : public QMainWindow, private Ui::MainWindow {

    Q_OBJECT

public:

    // Default constructor
    MdaBrowser (QWidget *parent = 0) ;
    
    virtual ~MdaBrowser();

    /**
     *  Returns current mode for displaying canvas
     */
    int getCanvasMode() const;
    
protected:
    
    // override closeEvent to store settings 
    virtual void closeEvent(QCloseEvent* event); 
    
private slots:

    // change how toolbar is displayed
    void changeToolbarView(QAction*);

    // change how right pane is displayed
    void changeViewMode(QAction*);

    // update view based on tree selection
    void treeSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
    
    // go up one folder
    void goUp();

    /// "open" item in right-hand panel
    void launch(const QModelIndex& index);

    // run configuration dialog
    void showConfigDialog();

    // run LB Tool
    void showLbTool();

    /// save main window geometry
    void saveGeometry();

    /// restore main window geometry
    void restoreGeometry();

private:

    /// plot one histogram
    void plot(const daq::mda::HistoFile& hfile);

    /// process time events
    void timerEvent(QTimerEvent* event);

    DbConnections* m_dbconn;
    MdaItemModel* m_mdaModel;
    TreeProxyModel* m_treeModel;
    ViewProxyModel* m_viewModel;
    FileCache m_fileCache;
    CanvasMgr* m_canvasMgr;
    QItemSelectionModel* m_treeSelectionModel;
    QItemSelectionModel* m_viewSelectionModel;
    QLabel* m_sbPartition;
    QLabel* m_sbRun;
    QLabel* m_sbServer;
    QLabel* m_sbProvider;
    QLabel* m_sbPath;
    int m_rootTimerId;

    // Copy constructor and assignment are disabled by default
    MdaBrowser ( const MdaBrowser& ) ;
    MdaBrowser& operator = ( const MdaBrowser& ) ;

};

} // namespace mda_browser

#endif // MDA_BROWSER_MDABROWSER_H
