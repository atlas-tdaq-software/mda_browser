#ifndef MDA_BROWSER_CANVASWIDGET_H
#define MDA_BROWSER_CANVASWIDGET_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include <QWidget>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class TCanvas;

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace mda_browser {

/// @addtogroup mda_browser

/**
 *  @ingroup mda_browser
 *
 *  @breief Widget which displays histograms in ROOT canvas
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class CanvasWidget: public QWidget {

    Q_OBJECT

public:

    // Default constructor
    CanvasWidget(QWidget* parent = 0);

    // Destructor
    virtual ~CanvasWidget();

    // get canvas instance
    TCanvas* canvas() const { return m_canvas; }

    // Have to override this method because we use WA_PaintOnScreen
    QPaintEngine* paintEngine() const override { return 0; }

protected:

    virtual void changeEvent(QEvent* e);

    virtual void hideEvent(QHideEvent * event);

    virtual void mouseMoveEvent(QMouseEvent* event);

    virtual void mousePressEvent(QMouseEvent* event);

    virtual void mouseReleaseEvent(QMouseEvent* event);

    virtual void paintEvent(QPaintEvent* event);

    virtual void resizeEvent(QResizeEvent* event);

    virtual void showEvent(QShowEvent * event);

public slots:

    // Delete all contents from a canvas
    void clear();

private:

    TCanvas* m_canvas;
    int m_wid;

};

} // namespace mda_browser

#endif // MDA_BROWSER_CANVASWIDGET_H
