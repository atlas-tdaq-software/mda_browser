//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ConfigDialog.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda_browser/Config.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ConfigDialog::ConfigDialog (QWidget* parent)
    : QDialog(parent)
{
    setupUi(this);

    // listbox needs a bit of adjustment
    int w = m_pageSwitchList->sizeHintForColumn(0) + 2*m_pageSwitchList->spacing();
    m_pageSwitchList->setMinimumWidth(w);
    m_pageSwitchList->setMaximumWidth(w);

    // populate all config items
    reset(0);
    reset(1);

    connect(m_buttonBox, SIGNAL(clicked(QAbstractButton*)), SLOT(buttonClicked(QAbstractButton*)));
    connect(this, SIGNAL(accepted()), SLOT(save()));
}

//--------------
// Destructor --
//--------------
ConfigDialog::~ConfigDialog ()
{
}

// set default values for current page
void
ConfigDialog::setDefaults(int page)
{
    Config config;

    switch (page) {
    case 0:
        lineEdit_MDA_DB->setText(config.defaultValue("database/mda_conn_str").toString());
        lineEdit_CoCa_DB->setText(config.defaultValue("database/coca_conn_str").toString());
        nRunsSpinBox->setValue(config.defaultValue("database/number_of_runs").toInt());
        break;
    case 1:
        lbToolMaxEntriesSpinBox->setValue(config.defaultValue("lb_tool/max_entries").toInt());
        lbToolFactorSpinBox->setValue(config.defaultValue("lb_tool/def_factor").toDouble());
        lbToolPrevFactorSpinBox->setValue(config.defaultValue("lb_tool/def_prev_factor").toDouble());
        int idx = lbToolOpComboBox->findText(config.defaultValue("lb_tool/def_op").toString());
        if (idx >= 0) lbToolOpComboBox->setCurrentIndex(idx);
        lbToolMRUSizeSpinBox->setValue(config.defaultValue("lb_tool/mru_size").toInt());
        break;
    }
}

// reset page to values in config
void
ConfigDialog::reset(int page)
{
    Config config;

    switch (page) {
    case 0:
        lineEdit_MDA_DB->setText(config.value("database/mda_conn_str").toString());
        lineEdit_CoCa_DB->setText(config.value("database/coca_conn_str").toString());
        nRunsSpinBox->setValue(config.value("database/number_of_runs").toInt());
        break;
    case 1:
        lbToolMaxEntriesSpinBox->setValue(config.value("lb_tool/max_entries").toInt());
        lbToolFactorSpinBox->setValue(config.value("lb_tool/def_factor").toDouble());
        lbToolPrevFactorSpinBox->setValue(config.value("lb_tool/def_prev_factor").toDouble());
        int idx = lbToolOpComboBox->findText(config.value("lb_tool/def_op").toString());
        if (idx >= 0) lbToolOpComboBox->setCurrentIndex(idx);
        lbToolMRUSizeSpinBox->setValue(config.value("lb_tool/mru_size").toInt());
        break;
    }
}

// process button click
void
ConfigDialog::buttonClicked(QAbstractButton* button)
{
    switch (m_buttonBox->standardButton(button)) {
    case QDialogButtonBox::RestoreDefaults:
        setDefaults(m_pageSwitchList->currentRow());
        break;
    case QDialogButtonBox::Reset:
        reset(m_pageSwitchList->currentRow());
        break;
    default:
        break;
    }
}

// save all current values to config
void
ConfigDialog::save()
{
    Config config;

    if (lineEdit_MDA_DB->text().isEmpty()) {
        config.setValue("database/mda_conn_str", QVariant());
    } else {
        config.setValue("database/mda_conn_str", lineEdit_MDA_DB->text());
    }
    if (lineEdit_CoCa_DB->text().isEmpty()) {
        config.setValue("database/coca_conn_str", QVariant());
    } else {
        config.setValue("database/coca_conn_str", lineEdit_CoCa_DB->text());
    }
    config.setValue("database/number_of_runs", nRunsSpinBox->value());

    config.setValue("lb_tool/max_entries", lbToolMaxEntriesSpinBox->value());
    config.setValue("lb_tool/def_factor", lbToolFactorSpinBox->value());
    config.setValue("lb_tool/def_prev_factor", lbToolPrevFactorSpinBox->value());
    config.setValue("lb_tool/def_op", lbToolOpComboBox->currentText());
    config.setValue("lb_tool/mru_size", lbToolMRUSizeSpinBox->value());
}

} // namespace mda_browser
