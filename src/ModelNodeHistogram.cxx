//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeHistogram.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/LB.h"
#include "mda_browser/ModelNodeFolder.h"
#include "mda_browser/ModelNodeLB.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeHistogram::ModelNodeHistogram (const QString& name,
        const std::set<daq::mda::HistoFile>& histos)
    : ModelNode()
    , m_name(name)
    , m_histo()
    , m_lbString()
{
    std::vector<boost::shared_ptr<ModelNode> > children;
    if (histos.size() == 1) {

        m_histo = *histos.begin();
        m_lbString = daq::mda::LB::lb2string(m_histo.lb()).c_str();

    } if (histos.size() > 1) {

        // get min/max LB
        unsigned lbmin = 0;
        unsigned lbmax = 0;
        typedef std::set<daq::mda::HistoFile>::const_iterator Iter;
        for (Iter it = histos.begin(); it != histos.end(); ++ it) {
            const daq::mda::HistoFile& histo = *it;
            if (histo.lb() < lbmin) lbmin = histo.lb();
            if (histo.lb() > lbmax) lbmax = histo.lb();
        }
        m_lbString = daq::mda::LB::lb2string(lbmin).c_str();
        m_lbString += "-";
        m_lbString += daq::mda::LB::lb2string(lbmax).c_str();

        // we only need LB number here
        m_histo = daq::mda::HistoFile("", lbmin, "", "", "", "");

        // add all LB children
        children.reserve(histos.size());
        for (Iter it = histos.begin(); it != histos.end(); ++ it) {
            children.push_back(boost::make_shared<ModelNodeLB>(*it));
        }

    }

    populate(children);
}

//--------------
// Destructor --
//--------------
ModelNodeHistogram::~ModelNodeHistogram ()
{
}

/// get absolute folder name, empty string for top-level folder
QString
ModelNodeHistogram::path() const
{
    return static_cast<ModelNodeFolder*>(parent())->path() + "/" + m_name;
}

/// returns true if node is a leaf node
bool
ModelNodeHistogram::leaf() const
{
    return rowCount() == 0;
}

/// Returns number of columns for children
int
ModelNodeHistogram::columnCount() const
{
    return ModelNodeLB::classColumnCount();
}

/// Returns column header for children
QString
ModelNodeHistogram::columnHeader(int column) const
{
    return ModelNodeLB::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodeHistogram::columnValue(int column) const
{
    switch(column) {
    case 0:
        return QVariant(m_name);
    case 1:
        return QVariant(m_lbString);
    case 2:
        return QVariant(m_histo.file().c_str());
    case 3:
        return QVariant(m_histo.histoPath().c_str());
    case 4:
        return QVariant(m_histo.archive().c_str());
    default:
        return QVariant();
    }
}

/// Return data for a given column used for sorting
QVariant
ModelNodeHistogram::sortValue(int column) const
{
    return column == 1 ? QVariant(m_histo.lb()) : columnValue(column);
}

/// Return icon for this object
const QIcon&
ModelNodeHistogram::icon() const
{
    static QIcon icon;
    static QIcon many_icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/chart_16x16.png");
        icon.addFile(":/Icons/images/chart_32x32.png");
        many_icon.addFile(":/Icons/images/chart_chain_16x16.png");
        many_icon.addFile(":/Icons/images/chart_chain_32x32.png");
    }
    if (rowCount() > 1) {
      return many_icon;
    } else {
      return icon;
    }
}


/// Load or refresh children data from database
void
ModelNodeHistogram::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>&,
        std::vector<boost::shared_ptr<ModelNode> >&)
{
    return;
}

// column header for this class
QString
ModelNodeHistogram::classColumnHeader(int column)
{
    static const char* headers[] = { "Name", "LumiBlocks", "File Name", "Histogram Path", "Archive Name" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
