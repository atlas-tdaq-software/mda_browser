//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/CanvasWidget.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <TCanvas.h>
#include <TVirtualX.h>
#include <QDebug>
#include <QMouseEvent>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
CanvasWidget::CanvasWidget (QWidget* parent)
    : QWidget(parent)
    , m_canvas(0)
    , m_wid(-1)
{
    // some special attributes on central widget to make it integrate better with ROOT canvas
    setUpdatesEnabled(false);
    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setMouseTracking(true);

    // Add this widget to TVirtualX list of windows
    // in return get its TVirtualX identifier
    WId x11WId = winId();
//    qDebug() << "CanvasWidget: winId =" << x11WId;
    m_wid = gVirtualX->AddWindow((ULong_t)x11WId, width(), height());

    m_canvas = new TCanvas("canvas_widget", width(), height(), m_wid);

    // workaround for QtRoot bug:
    // http://root.cern.ch/phpBB3/viewtopic.php?f=3&t=13426&start=30
    m_canvas->SetFillStyle(4100);
    m_canvas->SetFillColor(10);
}

//--------------
// Destructor --
//--------------
CanvasWidget::~CanvasWidget ()
{
//    qDebug() << "CanvasWidget: destructor() mapped = " << int(testAttribute(Qt::WA_Mapped));
    delete m_canvas;
    gVirtualX->RemoveWindow(m_wid);
}


// Delete all contents from a canvas
void 
CanvasWidget::clear()
{
    m_canvas->Clear();

    m_canvas->Modified(kTRUE);
    m_canvas->Update();
}

void
CanvasWidget::changeEvent(QEvent* /*event*/)
{
//    qDebug() << "CanvasWidget: changeEvent() mapped = " << int(testAttribute(Qt::WA_Mapped));
    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

void
CanvasWidget::hideEvent(QHideEvent * /*event*/)
{
//    qDebug() << "CanvasWidget: hideEvent() mapped = " << int(testAttribute(Qt::WA_Mapped));
}

void
CanvasWidget::mouseMoveEvent(QMouseEvent* event)
{
    if (not m_canvas) return;

    const QPoint& pos = event->pos();

    EEventType etype = kMouseMotion;
    if (event->buttons() & Qt::LeftButton) {
        if (event->modifiers() & Qt::ShiftModifier) {
            etype = kButton1ShiftMotion;
        } else {
            etype = kButton1Motion;
        }
    } else if (event->buttons() & Qt::MiddleButton) {
        etype = kButton2Motion;
    } else if (event->buttons() & Qt::RightButton) {
        etype = kButton3Motion;
    }
//    qDebug() << "CanvasWidget: mouseMoveEvent(): type =" << etype << " pos = " << pos;
    m_canvas->HandleInput(etype, pos.x(), pos.y());
}

void
CanvasWidget::mousePressEvent(QMouseEvent* event)
{
    if (not m_canvas) return;

    const QPoint& pos = event->pos();

    EEventType etype = kNoEvent;
    switch (event->button()) {
    case Qt::LeftButton:
        etype = kButton1Down;
        break;
    case Qt::MiddleButton:
        etype = kButton2Down;
        break;
    case Qt::RightButton:
        etype = kButton3Down;
        break;
    default:
        break;
    }
//    qDebug() << "CanvasWidget: mousePressEvent(): type =" << etype << " pos = " << pos;
    m_canvas->HandleInput(etype, pos.x(), pos.y());
}

void
CanvasWidget::mouseReleaseEvent(QMouseEvent* event)
{
    if (not m_canvas) return;

    const QPoint& pos = event->pos();

    EEventType etype = kNoEvent;
    switch (event->button()) {
    case Qt::LeftButton:
        etype = kButton1Up;
        break;
    case Qt::MiddleButton:
        etype = kButton2Up;
        break;
    case Qt::RightButton:
        etype = kButton3Up;
        break;
    default:
        break;
    }
//    qDebug() << "CanvasWidget: mouseReleaseEvent(): type =" << etype << " pos = " << pos;
    m_canvas->HandleInput(etype, pos.x(), pos.y());
}

void
CanvasWidget::paintEvent(QPaintEvent* /*event*/)
{
//    qDebug() << "CanvasWidget: paintEvent() mapped = " << int(testAttribute(Qt::WA_Mapped));

    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

void
CanvasWidget::showEvent(QShowEvent * /*event*/)
{
//    qDebug() << "CanvasWidget: showEvent() mapped = " << int(testAttribute(Qt::WA_Mapped));
    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

void
CanvasWidget::resizeEvent(QResizeEvent* /*event*/)
{
//    qDebug() << "CanvasWidget: resizeEvent() mapped = " << int(testAttribute(Qt::WA_Mapped));
    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

} // namespace mda_browser
