//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeSystem.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>
#include <QMessageBox>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNodePartition.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeSystem::ModelNodeSystem ()
    : ModelNode(0, 0)
{
}

//--------------
// Destructor --
//--------------
ModelNodeSystem::~ModelNodeSystem ()
{
}

/// Returns number of columns for children
int
ModelNodeSystem::columnCount() const
{
    return ModelNodePartition::classColumnCount();
}

/// Returns column header for children
QString
ModelNodeSystem::columnHeader(int column) const
{
    return ModelNodePartition::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodeSystem::columnValue(int column) const
{
    return column == 0 ? QVariant("Histogram Archive") : QVariant();
}

/// Return icon for this object
const QIcon&
ModelNodeSystem::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/archive_16x16.png");
        icon.addFile(":/Icons/images/archive_chart_32x32.png");
    }
    return icon;
}

/// Load or refresh children data from database
void
ModelNodeSystem::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
        std::vector<boost::shared_ptr<ModelNode> >& children)
{
    if (not mdaDb) return;

    // get data from database
    std::set<std::string> partitions;
    try {
        mdaDb->partitions(partitions);
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        QString msg = "Failed to retrieve OH partitions information from database:\n%1";
        QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
        return;
    }

    // instantiate all children objects
    children.reserve(partitions.size());
    for (std::set<std::string>::iterator it = partitions.begin(); it != partitions.end(); ++ it) {
        children.push_back(boost::make_shared<ModelNodePartition>(it->c_str()));
    }
}

// column header for this class
QString
ModelNodeSystem::classColumnHeader(int column)
{
    static const char* headers[] = { "Histogram Archive" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
