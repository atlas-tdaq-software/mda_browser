//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeLB.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/LB.h"
#include "mda_browser/ModelNodeHistogram.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeLB::ModelNodeLB (const daq::mda::HistoFile& histo)
    : ModelNode()
    , m_histo(histo)
{
    setPopulated(true);
}

//--------------
// Destructor --
//--------------
ModelNodeLB::~ModelNodeLB ()
{
}

/// returns true if node is a leaf node
bool
ModelNodeLB::leaf() const
{
    return true;
}

/// Returns number of columns for children
int
ModelNodeLB::columnCount() const
{
    return 0;
}

/// Returns column header for children
QString
ModelNodeLB::columnHeader(int) const
{
    return QString();
}

/// Return data for a given column
QVariant
ModelNodeLB::columnValue(int column) const
{
    switch(column) {
    case 0:
        return QVariant(daq::mda::LB::lb2string(m_histo.lb()).c_str());
    case 1:
        return QVariant(m_histo.file().c_str());
    case 2:
        return QVariant(m_histo.histoPath().c_str());
    case 3:
        return QVariant(m_histo.dataset().c_str());
    case 4:
        return QVariant(m_histo.archive().c_str());
    default:
        return QVariant();
    }
}
/// Return data for a given column used for sorting
QVariant
ModelNodeLB::sortValue(int column) const
{
    return column == 0 ? QVariant(m_histo.lb()) : columnValue(column);
}

/// Return icon for this object
const QIcon&
ModelNodeLB::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/chart_16x16.png");
        icon.addFile(":/Icons/images/chart_32x32.png");
    }
    return icon;
}


/// Load or refresh children data from database
void
ModelNodeLB::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>&,
        std::vector<boost::shared_ptr<ModelNode> >&)
{
    return;
}

// column header for this class
QString
ModelNodeLB::classColumnHeader(int column)
{
    static const char* headers[] = {
            "Lumi Block", "File Name", "Histogram Path", "Dataset", "Archive Name" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
