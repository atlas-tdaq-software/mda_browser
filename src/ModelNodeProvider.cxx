//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeProvider.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNodeFolder.h"
#include "mda_browser/ModelNodeHistogram.h"
#include "mda_browser/ModelNodePartition.h"
#include "mda_browser/ModelNodeRun.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeProvider::ModelNodeProvider (const QString& provider, const QString& server)
    : ModelNodeFolder(QString())
    , m_provider(provider)
    , m_server(server)
{
}

//--------------
// Destructor --
//--------------
ModelNodeProvider::~ModelNodeProvider ()
{
}

/// get partition name
QString
ModelNodeProvider::partition() const
{
    ModelNodeRun* run = static_cast<ModelNodeRun*>(parent());
    ModelNodePartition* part = static_cast<ModelNodePartition*>(run->parent());
    return part->partition();
}

/// get run number
unsigned
ModelNodeProvider::run() const
{
    ModelNodeRun* run = static_cast<ModelNodeRun*>(parent());
    return run->runInfo().run();
}

/// get server name
QString
ModelNodeProvider::server() const
{
    return m_server;
}

/// get provider name
QString
ModelNodeProvider::provider() const
{
    return m_provider;
}

/// Returns number of columns for children
int
ModelNodeProvider::columnCount() const
{
    return ModelNodeHistogram::classColumnCount();
}

/// Returns column header for children
QString
ModelNodeProvider::columnHeader(int column) const
{
    return ModelNodeHistogram::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodeProvider::columnValue(int column) const
{
    switch(column) {
    case 0:
        return QVariant(m_provider);
    case 1:
        return QVariant(m_server);
    default:
        return QVariant();
    }
}

/// Return icon for this object
const QIcon&
ModelNodeProvider::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/folder_rss_16x16.png");
        icon.addFile(":/Icons/images/folder_rss_32x32.png");
    }
    return icon;
}

// column header for this class
QString
ModelNodeProvider::classColumnHeader(int column)
{
    static const char* headers[] = { "Provider", "Server" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
