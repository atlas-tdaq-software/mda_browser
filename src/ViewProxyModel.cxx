//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ViewProxyModel.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <QDateTime>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    struct CompareData {

        // returns true if l is "less than" r
        static bool compare(const QVariant& l, const QVariant& r) {

            // null is less than non-null, two nulls are equal
            if (l.isNull()) return not r.isNull();
            if (r.isNull()) return false;

            // different types compare as type numbers
            if (l.userType() != r.userType()) return l.userType() < r.userType();

            // types are identical here
            switch (l.userType()) {
            case QVariant::Invalid:
                return (r.type() == QVariant::Invalid);
            case QVariant::Int:
                return l.toInt() < r.toInt();
            case QVariant::UInt:
                return l.toUInt() < r.toUInt();
            case QVariant::LongLong:
                return l.toLongLong() < r.toLongLong();
            case QVariant::ULongLong:
                return l.toULongLong() < r.toULongLong();
            case QMetaType::Float:
                return l.toFloat() < r.toFloat();
            case QVariant::Double:
                return l.toDouble() < r.toDouble();
            case QVariant::Char:
                return l.toChar() < r.toChar();
            case QVariant::Date:
                return l.toDate() < r.toDate();
            case QVariant::Time:
                return l.toTime() < r.toTime();
            case QVariant::DateTime:
                return l.toDateTime() < r.toDateTime();
            case QVariant::String:
            default:
                return l.toString().compare(r.toString()) < 0;
            }
        }
    };

    struct Compare {
        
        Compare(QAbstractItemModel* src_model, const QModelIndex& src_parent,  
                int sort_column, int sort_role, Qt::SortOrder order)
            : m_src_model(src_model), m_src_parent(src_parent)
            , m_sort_column(sort_column), m_sort_role(sort_role), m_order(order)
        {}
        
        bool operator()(int row1, int row2) const {
            QModelIndex src1 = m_src_model->index(row1, m_sort_column, m_src_parent);
            QModelIndex src2 = m_src_model->index(row2, m_sort_column, m_src_parent);
            const QVariant& data1 = m_src_model->data(src1, m_sort_role);
            const QVariant& data2 = m_src_model->data(src2, m_sort_role);
            if (m_order == Qt::AscendingOrder) {
                return CompareData::compare(data1, data2);
            } else {
                return CompareData::compare(data2, data1);                    
            }
        }
        
        QAbstractItemModel* m_src_model;
        const QModelIndex& m_src_parent;
        int m_sort_column;
        int m_sort_role;
        Qt::SortOrder m_order;
    };


}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ViewProxyModel::ViewProxyModel (QObject* parent)
    : QAbstractProxyModel(parent)
    , m_s2p()
    , m_p2s()
    , m_source_root()
    , m_column_count(0)
    , m_row_count(0)
    , m_sort_column(-1)
    , m_sort_order(Qt::AscendingOrder)
    , m_sort_role(Qt::DisplayRole)
{
}

//--------------
// Destructor --
//--------------
ViewProxyModel::~ViewProxyModel ()
{
}

// Returns the number of columns for the children of the given parent
int
ViewProxyModel::columnCount(const QModelIndex& parent) const
{
    if (not parent.isValid()) {
        // make sure that we have a mapping
        return sourceModel()->columnCount(m_source_root);
    }
    return 0;
}

// Returns the index of the item in the model specified by the given row, column and parent index.
QModelIndex
ViewProxyModel::index(int row, int column, const QModelIndex& parent) const
{
    if (not parent.isValid()) {
        // make sure that we have a mapping
        if (row < int(m_p2s.size())) {
            return createIndex(row, column);
        }
    }
    return QModelIndex();
}

// Returns the parent of the model item with the given index
QModelIndex
ViewProxyModel::parent(const QModelIndex&) const
{
    return QModelIndex();
}

// Returns the number of rows under the given parent
int
ViewProxyModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return 0;
    } else {
        return sourceModel()->rowCount(m_source_root);
    }
}

// return the model index in the proxy model that corresponds to the sourceIndex from the source model
QModelIndex
ViewProxyModel::mapFromSource(const QModelIndex& sourceIndex) const
{
    if (sourceIndex.isValid()) {
        int srcRow = sourceIndex.row();
        if (srcRow < int(m_s2p.size())) {
            return createIndex(m_s2p[srcRow], sourceIndex.column());
        }
    }
    return QModelIndex();
}

// return the model index in the source model that corresponds to the proxyIndex in the proxy model
QModelIndex
ViewProxyModel::mapToSource(const QModelIndex& proxyIndex) const
{
    if (proxyIndex.isValid()) {
        int pxyRow = proxyIndex.row();
        if (pxyRow < int(m_p2s.size())) {
            return sourceModel()->index(m_p2s[pxyRow], proxyIndex.column(), m_source_root);
        }
    }
    return m_source_root;
}

// Sets the given sourceModel to be processed by the proxy model
void 
ViewProxyModel::setSourceModel(QAbstractItemModel* sourceModel)
{
    QAbstractProxyModel::setSourceModel(sourceModel);
    setSourceRoot(QModelIndex());
}

// attaches current model to a different root in parent node
void
ViewProxyModel::setSourceRoot(const QModelIndex& source_index)
{
    QModelIndex none;
    ERS_DEBUG(1, "ViewProxyModel::setSourceRoot: 1 #persistentIndex=" << persistentIndexList().size());
    for (auto row = 0; row != m_row_count; ++ row) {
        for (auto col = 0; col != m_column_count; ++ col) {
            changePersistentIndex(createIndex(row, col), none);
        }
    }
    ERS_DEBUG(1, "ViewProxyModel::setSourceRoot: 2 #persistentIndex=" << persistentIndexList().size());

    if (m_row_count) {
        beginRemoveRows(none, 0, m_row_count-1);
        m_s2p.clear();
        m_p2s.clear();
        m_row_count = 0;
        endRemoveRows();
    }
    if (m_column_count) {
        beginRemoveColumns(none, 0, m_column_count-1);
        m_column_count = 0;
        endRemoveColumns();
    }
    m_source_root = source_index;
    populateMap();
}

void
ViewProxyModel::populateMap()
{
    m_column_count = sourceModel()->columnCount(m_source_root);
    beginInsertColumns(QModelIndex(), 0, m_column_count-1);
    endInsertColumns();

    m_row_count = sourceModel()->rowCount(m_source_root);
    beginInsertRows(QModelIndex(), 0, m_row_count-1);

    // fill map in natural order
    m_p2s.reserve(m_row_count);
    m_s2p.reserve(m_row_count);
    for (int i = 0; i != m_row_count; ++ i) {
        m_p2s.push_back(i);
        m_s2p.push_back(i);
    }
    
    endInsertRows();

    // sort it
    if (m_sort_column > m_column_count) {
        m_sort_column = -1;
        m_sort_order = Qt::AscendingOrder;
    }
    //sort(m_sort_column, m_sort_order);

}

// Sorts the model by column in the given order
void 
ViewProxyModel::sort(int column, Qt::SortOrder order)
{
    ERS_DEBUG(0, "ViewProxyModel::sort: column=" << column << " order=" << order);
    
    m_sort_column = column;
    m_sort_order = order;
    
    emit layoutAboutToBeChanged();

    if (column < 0) {
        // restore original order
        for (int i = 0; i != m_row_count; ++ i) {
            m_p2s[i] = i;
            m_s2p[i] = i;
        }
    } else {
        // sort according to column
        ::Compare comp(sourceModel(), m_source_root, m_sort_column, m_sort_role, m_sort_order);
        std::sort(m_p2s.begin(), m_p2s.end(), comp);

        // rebuild reverse map
        for (int i = 0; i != int(m_p2s.size()); ++ i) {
             m_s2p[m_p2s[i]] = i;
        }
    }
    

    emit layoutChanged();
}

} // namespace mda_browser
