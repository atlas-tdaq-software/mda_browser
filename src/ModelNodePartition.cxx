//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodePartition.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>
#include <QMessageBox>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/Config.h"
#include "mda_browser/ModelNodeRun.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodePartition::ModelNodePartition (const QString& name)
    : ModelNode()
    , m_name(name)
{
}

//--------------
// Destructor --
//--------------
ModelNodePartition::~ModelNodePartition ()
{
}

/// Returns number of columns for children
int
ModelNodePartition::columnCount() const
{
    return ModelNodeRun::classColumnCount();
}

/// Returns column header for children
QString
ModelNodePartition::columnHeader(int column) const
{
    return ModelNodeRun::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodePartition::columnValue(int column) const
{
    return column == 0 ? QVariant(m_name) : QVariant();
}

/// Return icon for this object
const QIcon&
ModelNodePartition::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/partition_16x16.png");
        icon.addFile(":/Icons/images/partition_32x32.png");
    }
    return icon;
}


/// Load or refresh children data from database
void
ModelNodePartition::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
        std::vector<boost::shared_ptr<ModelNode> >& children)
{
    ERS_DEBUG(0, "ModelNodePartition::populate: partition: " << qPrintable(m_name));

    if (not mdaDb) return;

    // get data from database
    std::set<daq::mda::RunInfo> last_runs;
    try {
        Config config;
        int maxRuns = config.value("database/number_of_runs", 0).toInt();
        if (maxRuns == 0) maxRuns = 1000000;
        mdaDb->runs(last_runs, qPrintable(m_name), maxRuns);
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        QString msg = "Failed to retrieve runs information from database:\n%1";
        QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
        return;
    }

    // add them in reverse order, most recent will show up first
    children.reserve(last_runs.size());
    typedef std::set<daq::mda::RunInfo>::const_reverse_iterator Iter;
    for (Iter it = last_runs.rbegin(); it != last_runs.rend(); ++ it) {
        children.push_back(boost::make_shared<ModelNodeRun>(*it));
    }
}

// column header for this class
QString
ModelNodePartition::classColumnHeader(int column)
{
    static const char* headers[] = { "Partition Name" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
