//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/MdaItemModel.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <QCursor>
#include <QApplication>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNode.h"
#include "mda_browser/ModelNodeSystem.h"
#include "mda_browser/utils.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    struct CursorGuard {
        CursorGuard(Qt::CursorShape shape = Qt::BusyCursor) {
            QApplication::setOverrideCursor(QCursor(shape));
        }
        ~CursorGuard() { QApplication::restoreOverrideCursor(); }
    };

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
MdaItemModel::MdaItemModel(DbConnections* dbconn, QObject* parent)
    : QAbstractItemModel(parent)
    , m_dbconn(dbconn)
    , m_rootNode(new ModelNodeSystem())
    , m_headers()
{
}

//--------------
// Destructor --
//--------------
MdaItemModel::~MdaItemModel ()
{
}

/// Returns true if node at given index has children
bool
MdaItemModel::hasChildren(const QModelIndex& parent) const
{
    if (not parent.isValid()) return true;

    const ModelNode* node = static_cast<const ModelNode*>(parent.internalPointer());
    return node and node->hasChildren();
}

/// Returns true for items that may need to bring data from database
bool
MdaItemModel::canFetchMore(const QModelIndex& parent) const
{
    if (not parent.isValid()) return false;

    const ModelNode* node = static_cast<const ModelNode*>(parent.internalPointer());
    return node and not node->populated();
}

/// get data from database or refresh current data
void
MdaItemModel::fetchMore(const QModelIndex& parent)
{
    ModelNode* node = static_cast<ModelNode*>(parent.internalPointer());
    if (node and not node->populated()) {
        std::vector<boost::shared_ptr<ModelNode> > children;
        CursorGuard cursorGuard;
        boost::shared_ptr<daq::mda::DBRead> mdaDb = m_dbconn->getMda();
        if (not mdaDb) return;
        node->fetchChildren(mdaDb, children);
        node->populate(children);
    }
}

/// get number of children for given node
int
MdaItemModel::rowCount(const QModelIndex& parent) const
{
    if (not parent.isValid()) return 1;

    const ModelNode* node = static_cast<const ModelNode*>(parent.internalPointer());
    return node ? node->rowCount() : 0;
}

/// get number of columns for the children of this node
int MdaItemModel::columnCount(const QModelIndex& parent) const
{
    // top-level entry
    if (not parent.isValid()) return 1;

    const ModelNode* node = static_cast<const ModelNode*>(parent.internalPointer());
    return node ? node->columnCount() : 0;
}

/// get item data
QVariant
MdaItemModel::data(const QModelIndex& index, int role) const
{
    const ModelNode* node = static_cast<const ModelNode*>(index.internalPointer());
    if (not node) return QVariant();

    if (role == Qt::DisplayRole) return node->columnValue(index.column());
    if (role == Qt::DecorationRole and index.column() == 0) return node->icon();
    // UserRole is used for sorting
    if (role == Qt::UserRole) return node->sortValue(index.column());
    return QVariant();
}

/// get header data
QVariant
MdaItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal and role == Qt::DisplayRole) {
        if (section >= 0 and section < int(m_headers.size())) return QVariant(m_headers[section]);
    }
    return QVariant();
}


/// get the index for the child of a given node
QModelIndex
MdaItemModel::index(int row, int column, const QModelIndex& parent) const
{
    // top-level entry
    if (not parent.isValid()) return createIndex(row, column, static_cast<void*>(m_rootNode.get()));

    const ModelNode* node = static_cast<const ModelNode*>(parent.internalPointer());
    ModelNode* child = node ? node->child(row) : 0;
    return child ? createIndex(row, column, static_cast<void*>(child)) : QModelIndex();
}

/// get the index for a given node
QModelIndex
MdaItemModel::index(ModelNode* node, int column) const
{
    return node ? createIndex(node->row(), column, static_cast<void*>(node)) : QModelIndex();
}

/// get the index of the parent of this node
QModelIndex
MdaItemModel::parent(const QModelIndex& index) const
{
    const ModelNode* node = static_cast<const ModelNode*>(index.internalPointer());
    ModelNode* parent = node ? node->parent() : 0;
    return parent ?  createIndex(parent->row(), 0, static_cast<void*>(parent)) : QModelIndex();
}

/// remove all children of the given parent
void
MdaItemModel::deleteChildren(const QModelIndex& index)
{
    ModelNode* node = static_cast<ModelNode*>(index.internalPointer());
    if (node) {
        int nrows = node->rowCount();
        if (nrows) {
            ERS_DEBUG(0, "MdaItemModel::deleteChildren");
            beginRemoveRows(index, 0, nrows-1);
            node->clearChildren();
            endRemoveRows();
        }
    }
}

/// remove all grand children of the given node
void
MdaItemModel::deleteGrandChildren(const QModelIndex& parent)
{
    ModelNode* node = static_cast<ModelNode*>(parent.internalPointer());
    if (node) {
        ERS_DEBUG(0, "MdaItemModel::deleteGrandChildren");
        for (int i = 0; i != node->rowCount(); ++ i) {
            deleteChildren(index(node->child(i)));
        }
    }
}


/// populate given parent with info from database
void
MdaItemModel::populate(const QModelIndex& index)
{
    ModelNode* node = static_cast<ModelNode*>(index.internalPointer());
    if (node and not node->populated()) {
        if (node->rowCount()) {
            // TODO: non-empty, may need to clear?
            return;
        }
        ERS_DEBUG(0, "MdaItemModel::populate: index=" << index);
        CursorGuard cursorGuard;
        boost::shared_ptr<daq::mda::DBRead> mdaDb = m_dbconn->getMda();
        if (not mdaDb) return;
        std::vector<boost::shared_ptr<ModelNode> > children;
        node->fetchChildren(mdaDb, children);
        if (not children.empty()) {
            beginInsertRows(index, 0, children.size()-1);
            node->populate(children);
            endInsertRows();
        }
    }
}

/// set model headers from given item
void
MdaItemModel::setHeaders(const QModelIndex& index)
{
    m_headers.clear();
    ModelNode* node = static_cast<ModelNode*>(index.internalPointer());
    if (node) {
        int ncol = node->columnCount();
        for (int col = 0; col != ncol; ++ col) {
            m_headers.push_back(node->columnHeader(col));
        }
    }
}

} // namespace mda_browser
