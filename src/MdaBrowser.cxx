//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/MdaBrowser.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <QCloseEvent>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <TApplication.h>
#include <TSystem.h>
#include <TH1.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda/common/LB.h"
#include "mda_browser/Config.h"
#include "mda_browser/ConfigDialog.h"
#include "mda_browser/LbToolDialog.h"
#include "mda_browser/LbToolEntry.h"
#include "mda_browser/MdaItemModel.h"
#include "mda_browser/ModelNode.h"
#include "mda_browser/ModelNodeFolder.h"
#include "mda_browser/ModelNodeHistogram.h"
#include "mda_browser/ModelNodeLB.h"
#include "mda_browser/ModelNodePartition.h"
#include "mda_browser/ModelNodeRun.h"
#include "mda_browser/utils.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    struct CursorGuard {
        CursorGuard(Qt::CursorShape shape = Qt::BusyCursor) {
            QApplication::setOverrideCursor(QCursor(shape));
        }
        ~CursorGuard() { QApplication::restoreOverrideCursor(); }
    };

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
MdaBrowser::MdaBrowser (QWidget *parent)
    : QMainWindow(parent)
    , Ui::MainWindow()
    , m_dbconn()
    , m_mdaModel()
    , m_fileCache()
    , m_canvasMgr(0)
    , m_rootTimerId(0)
{
    setupUi(this);

    m_dbconn = new DbConnections(this);
    
    // add few labels to statusbar
    m_sbPartition = new QLabel(m_statusbar);
    m_sbPartition->setToolTip("Partition name for currently selected item");
    m_sbRun = new QLabel(m_statusbar);
    m_sbRun->setToolTip("Run number for currently selected item");
    m_sbServer = new QLabel(m_statusbar);
    m_sbServer->setToolTip("OH server name for currently selected item");
    m_sbProvider = new QLabel(m_statusbar);
    m_sbProvider->setToolTip("OH provider name for currently selected item");
    m_sbPath = new QLabel(m_statusbar);
    m_sbPath->setToolTip("Folder or histogram path for\ncurrently selected item in a tree");
    m_statusbar->insertPermanentWidget(0, m_sbPartition);
    m_statusbar->insertPermanentWidget(1, m_sbRun);
    m_statusbar->insertPermanentWidget(2, m_sbServer);
    m_statusbar->insertPermanentWidget(3, m_sbProvider);
    m_statusbar->insertPermanentWidget(4, m_sbPath);

    // setup action group for exclusive toolbar mode
    QActionGroup* ag = new QActionGroup(this);
    ag->addAction(m_actionToolbarIconsOnly);
    ag->addAction(m_actionToolbarnTextOnly);
    ag->addAction(m_actionToolbarTextBelowIcons);
    ag->addAction(m_actionToolbarTextBesidesIcons);
    m_actionToolbarTextBelowIcons->setChecked(true);
    changeToolbarView(m_actionToolbarTextBelowIcons);
    connect(ag, SIGNAL(triggered(QAction*)), SLOT(changeToolbarView(QAction*)));

    // setup action group for exclusive view mode
    ag = new QActionGroup(this);
    ag->addAction(m_actionShowIcons);
    ag->addAction(m_actionShowList);
    ag->addAction(m_actionShowDetails);
    m_actionShowDetails->setChecked(true);
    changeViewMode(m_actionShowDetails);
    connect(ag, SIGNAL(triggered(QAction*)), SLOT(changeViewMode(QAction*)));

    // setup action group for exclusive canvas mode
    ag = new QActionGroup(this);
    ag->addAction(m_actionCanvasSingle);
    ag->addAction(m_actionCanvasNew);
    ag->addAction(m_actionCanvasNewPad);
    m_actionCanvasSingle->setChecked(true);

    // instantiate canvas manager 
    m_canvasMgr = new CanvasMgr(this);
    
    // setup models
    m_mdaModel = new MdaItemModel(m_dbconn, this);
    m_treeModel = new TreeProxyModel(this);
    m_treeModel->setSourceModel(m_mdaModel);
    m_treeModel->setFilterKeyColumn(0);
    m_viewModel = new ViewProxyModel(this);
    m_viewModel->setSourceModel(m_mdaModel);
    m_viewModel->setSortRole(Qt::UserRole);

    // init views
    m_treeView->setModel(m_treeModel);
    m_viewList->setModel(m_viewModel);
    m_viewTable->sortByColumn(-1, Qt::AscendingOrder);
    m_viewTable->setModel(m_viewModel);
    m_viewTable->resizeColumnToContents(0);

    m_treeSelectionModel = new QItemSelectionModel(m_treeModel, this);
    m_treeView->setSelectionModel(m_treeSelectionModel);

    m_viewSelectionModel = new QItemSelectionModel(m_viewModel, this);
    m_viewList->setSelectionModel(m_viewSelectionModel);
    m_viewTable->setSelectionModel(m_viewSelectionModel);

    connect(m_treeSelectionModel, SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
            SLOT(treeSelectionChanged(const QItemSelection&, const QItemSelection&)));

    // double-clicking
    connect(m_viewList, SIGNAL(activated(const QModelIndex&)), SLOT(launch(const QModelIndex&)));
    connect(m_viewTable, SIGNAL(activated(const QModelIndex&)), SLOT(launch(const QModelIndex&)));

    // dynamic model sometimes need help to understand that children are not
    // there when tree is expanded
    connect(m_treeView, SIGNAL(expanded(const QModelIndex&)), m_treeModel, SIGNAL(layoutChanged()));

    connect(m_actionPreferences, SIGNAL(triggered()), SLOT(showConfigDialog()));
    connect(m_actionLBTool, SIGNAL(triggered()), SLOT(showLbTool()));

    connect(m_actionUp, SIGNAL(triggered()), SLOT(goUp()));
    m_actionUp->setEnabled(false);

    restoreGeometry();

    // start timer for every 10 ms to process ROOT events
    m_rootTimerId = startTimer(10);
}

MdaBrowser::~MdaBrowser()
{
}

void MdaBrowser::changeToolbarView(QAction* action)
{
    if (action == m_actionToolbarIconsOnly) {
        m_toolbar->setToolButtonStyle(Qt::ToolButtonIconOnly);
    } else if (action == m_actionToolbarnTextOnly) {
        m_toolbar->setToolButtonStyle(Qt::ToolButtonTextOnly);
    } else if (action == m_actionToolbarTextBelowIcons) {
        m_toolbar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    } else if (action == m_actionToolbarTextBesidesIcons) {
        m_toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    }
}


void MdaBrowser::changeViewMode(QAction* action)
{
    if (action == m_actionShowIcons) {
        m_viewStack->setCurrentIndex(0);
        m_viewList->setViewMode(QListView::IconMode);
    } else if (action == m_actionShowList) {
        m_viewStack->setCurrentIndex(0);
        m_viewList->setViewMode(QListView::ListMode);
    } else if (action == m_actionShowDetails) {
        m_viewStack->setCurrentIndex(1);
    }
}

int
MdaBrowser::getCanvasMode() const
{
    if (m_actionCanvasSingle->isChecked()) {
        return 0;
    } else if (m_actionCanvasNew->isChecked()) {
        return 1;
    } else if (m_actionCanvasNewPad->isChecked()) {
        return 2;
    }

    return 0;
}

void
MdaBrowser::treeSelectionChanged(const QItemSelection& selected, const QItemSelection&)
{
    const QModelIndexList& items = selected.indexes();
    if (not items.empty()) {

        CursorGuard cursorGuard;

        ERS_DEBUG(1, "MdaBrowser::treeSelectionChanged, #selected=" << items.size()
                  << " items[0]=" << items[0]);

        // map it to source model
        const QModelIndex& index = m_treeModel->mapToSource(items.first());
        ERS_DEBUG(1, "MdaBrowser::treeSelectionChanged, mainIndex=" << index);

        // make sure model gets the data
        m_mdaModel->populate(index);

        // change model headers
        m_mdaModel->setHeaders(index);

        // change whatever is displayed in the list area
        m_viewModel->setSourceRoot(index);

        // after updating view content we need to update header sort indicator
        //m_viewTable->header()->setSortIndicator(m_viewModel->sortColumn(), m_viewModel->sortOrder());

        // resize all columns to fit
        for (int col = 0; col != m_mdaModel->columnCount(index); ++ col) {
          m_viewTable->resizeColumnToContents(col);
        }

        ModelNode* node = static_cast<ModelNode*>(index.internalPointer());

        // update up button state
        m_actionUp->setEnabled(node->parent() != 0);

        // update status bar
        m_sbPartition->clear();
        m_sbRun->clear();
        m_sbServer->clear();
        m_sbProvider->clear();
        m_sbPath->clear();
        if (ModelNodePartition* p = dynamic_cast<ModelNodePartition*>(node)) {
            m_sbPartition->setText(p->partition());
        } else if (ModelNodeRun* r = dynamic_cast<ModelNodeRun*>(node)) {
            m_sbPartition->setText(r->partition());
            m_sbRun->setText(QString("%1").arg(r->runInfo().run()));
        } else if (ModelNodeFolder* f = dynamic_cast<ModelNodeFolder*>(node)) {
            m_sbPartition->setText(f->partition());
            m_sbRun->setText(QString("%1").arg(f->run()));
            m_sbServer->setText(f->server());
            m_sbProvider->setText(f->provider());
            m_sbPath->setText(f->path());
        } else if (ModelNodeHistogram* h = dynamic_cast<ModelNodeHistogram*>(node)) {
            if (ModelNodeFolder* f = dynamic_cast<ModelNodeFolder*>(h->parent())) {
                m_sbPartition->setText(f->partition());
                m_sbRun->setText(QString("%1").arg(f->run()));
                m_sbServer->setText(f->server());
                m_sbProvider->setText(f->provider());
                m_sbPath->setText(h->path());
            }
        } else {
            statusBar()->clearMessage();
        }

    }
}

// go up one folder
void
MdaBrowser::goUp()
{
    // get current tree index
    QModelIndex curTreeIndex = m_treeView->currentIndex();
    if (not curTreeIndex.isValid()) return;

    // get its parent
    QModelIndex parent = m_treeModel->parent(curTreeIndex);
    if (not parent.isValid()) return;

    // set it as a current
    m_treeView->setCurrentIndex(parent);
}

/// save main window geometry
void 
MdaBrowser::saveGeometry()
{
    Config config;

    config.setValue("geometry/main_window", QMainWindow::saveGeometry());
}

/// restore main window geometry
void 
MdaBrowser::restoreGeometry()
{
    Config config;

    QMainWindow::restoreGeometry(config.value("geometry/main_window").toByteArray());
}

void
MdaBrowser::launch(const QModelIndex& index)
{
    // if item is a folder then make it current in the tree, otherwise we need to display histogram

    // map it to tree model
    const QModelIndex& mainIndex = m_viewModel->mapToSource(index);
    ERS_DEBUG(0, "MdaBrowser::launch: index=" << index << " mainIndex=" << mainIndex);
    ModelNode* node = static_cast<ModelNode*>(mainIndex.internalPointer());
    if (node) {
        if (not node->leaf()) {
            // move tree selection to this node
            const QModelIndex& treeIndex = m_treeModel->mapFromSource(mainIndex);
            ERS_DEBUG(0, "MdaBrowser::launch: treeIndex=" << treeIndex);
            m_treeView->setCurrentIndex(treeIndex);
        } else if (ModelNodeHistogram* h = dynamic_cast<ModelNodeHistogram*>(node)) {
            plot(h->hfile());
        } else if (ModelNodeLB* h = dynamic_cast<ModelNodeLB*>(node)) {
            plot(h->hfile());
        }
    }
}

void
MdaBrowser::showConfigDialog()
{
    ConfigDialog dialog;
    dialog.exec();
}

// run LB Tool
void
MdaBrowser::showLbTool()
{
    Config config;

    // populate entry list from selected data
    QString partition;
    QString server;
    QString provider;
    QString path;
    unsigned run = 0;
    std::vector<daq::mda::HistoFile> hfiles;

    if (m_viewSelectionModel->hasSelection()) {

        ERS_DEBUG(0, "MdaBrowser::showLbTool: view has selection");
        const QModelIndexList& selection = m_viewSelectionModel->selectedRows();
        for (QModelIndexList::const_iterator it = selection.begin(); it != selection.end(); ++ it) {

            const QModelIndex& mainIndex = m_viewModel->mapToSource(*it);
            const ModelNode* node = static_cast<const ModelNode*>(mainIndex.internalPointer());

            if (const ModelNodeHistogram* hnode = dynamic_cast<const ModelNodeHistogram*>(node)) {

                const ModelNodeFolder* folder = dynamic_cast<const ModelNodeFolder*>(hnode->parent());
                if (not folder) continue;

                if (partition.isEmpty()) {
                    partition = folder->partition();
                } else if (folder->partition() != partition) {
                    continue;
                }
                if (server.isEmpty()) {
                    server = folder->server();
                } else if (folder->server() != server) {
                    continue;
                }
                if (provider.isEmpty()) {
                    provider = folder->provider();
                } else if (folder->provider() != provider) {
                    continue;
                }
                if (run == 0) {
                    run = folder->run();
                } else if (folder->run() != run) {
                    continue;
                }
                if (path.isEmpty()) {
                    path = hnode->path();
                } else if (hnode->path() != path) {
                    continue;
                }

                if (hnode->leaf()) {
                    hfiles.push_back(hnode->hfile());
                } else {
                    // add all its children
                    const int children = hnode->rowCount();
                    for (int i = 0; i != children; ++ i) {
                        if (const ModelNodeLB* lbnode = dynamic_cast<const ModelNodeLB*>(hnode->child(i))) {
                            hfiles.push_back(lbnode->hfile());
                        }
                    }
                }

            } else if (const ModelNodeLB* lbnode = dynamic_cast<const ModelNodeLB*>(node)) {

                const ModelNodeHistogram* hnode = dynamic_cast<const ModelNodeHistogram*>(lbnode->parent());
                if (not hnode) continue;
                const ModelNodeFolder* folder = dynamic_cast<const ModelNodeFolder*>(hnode->parent());
                if (not folder) continue;

                if (partition.isEmpty()) {
                    partition = folder->partition();
                } else if (folder->partition() != partition) {
                    continue;
                }
                if (server.isEmpty()) {
                    server = folder->server();
                } else if (folder->server() != server) {
                    continue;
                }
                if (provider.isEmpty()) {
                    provider = folder->provider();
                } else if (folder->provider() != provider) {
                    continue;
                }
                if (run == 0) {
                    run = folder->run();
                } else if (folder->run() != run) {
                    continue;
                }
                if (path.isEmpty()) {
                    path = hnode->path();
                } else if (hnode->path() != path) {
                    continue;
                }

                hfiles.push_back(lbnode->hfile());

            }
        }

    } else if (m_treeSelectionModel->hasSelection()) {

        ERS_DEBUG(0, "MdaBrowser::showLbTool: tree has selection");
        const QModelIndexList& selection = m_treeSelectionModel->selectedRows();
        for (QModelIndexList::const_iterator it = selection.begin(); it != selection.end(); ++ it) {

            const QModelIndex& mainIndex = m_treeModel->mapToSource(*it);
            const ModelNode* node = static_cast<const ModelNode*>(mainIndex.internalPointer());

            if (const ModelNodeHistogram* hnode = dynamic_cast<const ModelNodeHistogram*>(node)) {

                const ModelNodeFolder* folder = dynamic_cast<const ModelNodeFolder*>(hnode->parent());
                if (not folder) continue;

                if (partition.isEmpty()) {
                    partition = folder->partition();
                } else if (folder->partition() != partition) {
                    continue;
                }
                if (server.isEmpty()) {
                    server = folder->server();
                } else if (folder->server() != server) {
                    continue;
                }
                if (provider.isEmpty()) {
                    provider = folder->provider();
                } else if (folder->provider() != provider) {
                    continue;
                }
                if (run == 0) {
                    run = folder->run();
                } else if (folder->run() != run) {
                    continue;
                }
                if (path.isEmpty()) {
                    path = hnode->path();
                } else if (hnode->path() != path) {
                    continue;
                }

                // add all its children
                const int children = hnode->rowCount();
                for (int i = 0; i != children; ++ i) {
                    if (const ModelNodeLB* lbnode = dynamic_cast<const ModelNodeLB*>(hnode->child(i))) {
                        hfiles.push_back(lbnode->hfile());
                    }
                }

            }

        }

    }

    // get default entry values from config
    double f = config.value("lb_tool/def_factor").toDouble();
    double fprev = config.value("lb_tool/def_prev_factor").toDouble();
    QString strop = config.value("lb_tool/def_op").toString();

    // make entries out of all LBs
    std::vector<LbToolEntry> entries;
    for (std::vector<daq::mda::HistoFile>::const_iterator it = hfiles.begin(); it != hfiles.end(); ++ it) {
        LbToolEntry entry(run, it->lb(), f, LbToolEntry::Minus, fprev,
                it->file().c_str(), it->dataset().c_str(), it->histoPath().c_str());
        entry.setOp(strop);
        if (std::find(entries.begin(), entries.end(), entry) != entries.end()) continue;
        entries.push_back(entry);
        ERS_DEBUG(0, "MdaBrowser::showLbTool: adding entry run=" << entry.run() << " lb=" << entry.lb());
    }

    // limit entries number
    int max_entries = config.value("lb_tool/max_entries").toInt();
    if (int(entries.size()) > max_entries) {
        entries.resize(max_entries);
    }

    LbToolDialog* dialog = new LbToolDialog(0, m_dbconn, &m_fileCache, partition, server, provider, path, entries);
    dialog->show();
}

/// plot one histogram
void
MdaBrowser::plot(const daq::mda::HistoFile& hfile)
{
    CursorGuard cursorGuard;

    boost::shared_ptr<daq::mda::FileRead> fileRead = m_dbconn->getFileRead();
    if (not fileRead) {
        // error message should have been displayed already
        return;
    }
    boost::shared_ptr<TFile> tfile = m_fileCache.getFile(hfile.file(), hfile.dataset(), fileRead);
    if (not tfile) {
        // display error message here
        ERS_LOG("MdaBrowser::plot: failed to open file " << hfile.file());
        QString msg = "Failed to locate or open file\n%1";
        QMessageBox::critical(this, "Cannot open file", msg.arg(hfile.file().c_str()));
        return;
    }
    ERS_DEBUG(0, "MdaBrowser::plot: opened file " << tfile->GetName());

    // read histogram, do not add it to memory directory
    TH1::AddDirectory(kFALSE);
    TObject* object = tfile->Get(hfile.histoPath().c_str());

    if (not object) {
        // display error message here
        ERS_LOG("MdaBrowser::plot: failed to locate object " << hfile.histoPath());
        QString msg = "Failed to locate histogram\n%1\nin a file %2";
        QMessageBox::critical(this, "Cannot find histogram", msg.arg(hfile.histoPath().c_str(), hfile.file().c_str()));
        return;
    } else {
        QString title("%1 [LB=%2]");
        title = title.arg(object->GetTitle(), daq::mda::LB::lb2string(hfile.lb()).c_str());
        ((TNamed*) object)->SetTitle(qPrintable(title));
    }

    CanvasWindow* canvas = 0;
    switch(getCanvasMode()) {
    case 0:
        // replace canvas contents
        canvas = m_canvasMgr->lastCanvas();
        canvas->clear();
        break;
    case 1:
        // make new canvas
        canvas = m_canvasMgr->newCanvas();
        break;
    case 2:
        // add to canvas
        canvas = m_canvasMgr->lastCanvas();
        break;
    }
    canvas->add(object);
    canvas->show();

}

void 
MdaBrowser::closeEvent(QCloseEvent *event)
{
    saveGeometry();
    QMainWindow::closeEvent(event);
}

/// process time events
void
MdaBrowser::timerEvent(QTimerEvent* event)
{
    if (event->timerId() == m_rootTimerId) {
        // process some ROOT stuff
        gSystem->ProcessEvents();
    }
}

} // namespace mda_browser
