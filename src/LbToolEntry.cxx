//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/LbToolEntry.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/LB.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
LbToolEntry::LbToolEntry ()
    : m_run(0)
    , m_lb(0)
    , m_f(1.)
    , m_fprev(1.)
    , m_op(Minus)
    , m_fileName()
    , m_histoPath()
{
}

LbToolEntry::LbToolEntry(uint32_t run, uint32_t lb, double f, Op op, double fprev, 
        const QString& fileName, const QString& dataset, const QString& histoPath)
    : m_run(run)
    , m_lb(lb)
    , m_f(f)
    , m_fprev(fprev)
    , m_op(op)
    , m_fileName(fileName)
    , m_dataset(dataset)
    , m_histoPath(histoPath)
{
}

QString
LbToolEntry::lbString() const
{
    return QString(daq::mda::LB::lb2string(m_lb).c_str());
}

void 
LbToolEntry::setRun(uint32_t run) 
{ 
    m_run = run;
    m_fileName.clear();
    m_dataset.clear();
    m_histoPath.clear();
}

void 
LbToolEntry::setLb(uint32_t lb) 
{ 
    m_lb = lb; 
    m_fileName.clear();
    m_dataset.clear();
    m_histoPath.clear();
}

bool
LbToolEntry::setLb(const QString& lb)
{
    if (lb.toLower() == "eor") {
        m_lb = daq::mda::LB::LastLB;
        m_fileName.clear();
        m_dataset.clear();
        m_histoPath.clear();
        return true;
    }
    bool ok;
    int lbint = lb.toInt(&ok);
    if (ok) {
        m_lb = lbint;
        m_fileName.clear();
        m_dataset.clear();
        m_histoPath.clear();
    }
    return ok;
}

bool
LbToolEntry::setOp(const QString& op)
{
    if (op == "-") {
        m_op = Minus;
        return true;
    } else if (op == "+") {
        m_op = Plus;
        return true;
    } else if (op == "*") {
        m_op = Multiply;
        return true;
    } else if (op == "/") {
        m_op = Divide;
        return true;
    }
    return false;
}

} // namespace mda_browser
