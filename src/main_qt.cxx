//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <stdlib.h>
#include <QApplication>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <TApplication.h>
#include <TError.h>
#include "mda_browser/MdaBrowser.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------



int main(int argc, char *argv[])
{
    // init ROOT application instance
    TApplication* rootApp = new TApplication("MDA Browser (ROOT)", &argc, argv);
    rootApp->SetReturnFromRun(true);
    gErrorIgnoreLevel = kError;

    // Workaround for Qt5 issue with g_main_context_pop_thread_default,
    // see https://its.cern.ch/jira/browse/ADHI-4551 for details.
    // Exact value of the variable does not matter, any non-empty string is OK.
    setenv("QT_NO_THREADED_GLIB", "ADHI-4551", 0);

    // make Qt app instance, this needs to be done _after_ ROOT initialization
    // to override ROOT-installed X11 error handler. Qt error handler knows how
    // to suppress errors due to broken TCanvas implementation.
    QApplication app(argc, argv);
    app.setOrganizationName("ATLAS");
    app.setOrganizationDomain("atlas.cern.ch");
    app.setApplicationName("MDA Browser");
    app.setWindowIcon(QIcon(":/Icons/images/many_histogram.xpm"));

    // main window
    mda_browser::MdaBrowser mainWin;
    mainWin.show();

    // run until done
    return app.exec();
}
