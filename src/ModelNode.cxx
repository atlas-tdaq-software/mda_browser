//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNode.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda_browser/MdaItemModel.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNode::ModelNode (ModelNode* parent, int row)
    : m_parent(parent)
    , m_row(row)
    , m_populated(false)
    , m_children()
{
}

//--------------
// Destructor --
//--------------
ModelNode::~ModelNode ()
{
}

/// returns true if node is a leaf node
bool
ModelNode::leaf() const
{
    return false;
}

/// Return data for a given column used for sorting,
/// default is to return columnValue()
QVariant
ModelNode::sortValue(int column) const
{
    return columnValue(column);
}

/// Add children nodes, call only if populated() returns false
void
ModelNode::populate(const std::vector<boost::shared_ptr<ModelNode> >& children)
{
    m_children.clear();
    m_children.reserve(m_children.size());
    std::vector<boost::shared_ptr<ModelNode> >::const_iterator it = children.begin();
    for (int row = 0; it != children.end(); ++ it, ++ row) {
        (*it)->setRow(row);
        (*it)->setParent(this);
        m_children.push_back(*it);
    }
    m_populated = true;
}

// remove all children
void
ModelNode::clearChildren()
{
    m_children.clear();
    m_populated = false;
}

} // namespace mda_browser
