//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/LbToolModel.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <stdexcept>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/LB.h"
#include "mda_browser/Config.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
LbToolModel::LbToolModel(QObject* parent)
    : QAbstractItemModel(parent)
    , m_initial()
    , m_data()
{
}

LbToolModel::LbToolModel(QObject* parent, const std::vector<LbToolEntry>& initial)
    : QAbstractItemModel(parent)
    , m_initial(initial)
    , m_data(initial)
{
}

//--------------
// Destructor --
//--------------
LbToolModel::~LbToolModel ()
{
}

// Returns the number of columns for the children of the given parent.
int
LbToolModel::columnCount(const QModelIndex&) const
{
    return 5;
}

// Returns the data stored under the given role for the item referred to by the index.
QVariant
LbToolModel::data(const QModelIndex& index, int role) const
{
    if (index.isValid() and index.row() < int(m_data.size()) and
            (role == Qt::DisplayRole || role == Qt::EditRole)) {

        const LbToolEntry& entry = m_data[index.row()];

        switch(index.column()) {
        case RunColumn:
            return QVariant(entry.run());
        case LbColumn:
            if (role == Qt::DisplayRole and entry.lb() == daq::mda::LB::LastLB) return QVariant("EoR");
            return QVariant(entry.lb());
        case FactorColumn:
            if (role == Qt::DisplayRole and entry.factor() == 0) return QVariant("Auto");
            return QVariant(entry.factor());
        case OpColumn:
            if (index.row() == 0) return QVariant();
            if (entry.factor() == 0) return QVariant(entry.opString());
            if (entry.prevFactor() == 0) return QVariant();
            return QVariant(entry.opString());
        case PrevFactorColumn:
            if (index.row() == 0) return QVariant();
            if (role == Qt::DisplayRole and entry.factor() == 0) return QVariant("Auto");
            return QVariant(entry.prevFactor());
        }
    }
    return QVariant();
}

// Returns the item flags for the given index.
Qt::ItemFlags
LbToolModel::flags(const QModelIndex& index) const
{
    if (!index.isValid()) return Qt::ItemIsEnabled;

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);

    // few cells in the very first row are not editable
    if (index.row() > 0 or (index.column() != OpColumn and index.column() != PrevFactorColumn)) {
        flags |= Qt::ItemIsEditable;
    }
    return flags;
}

// Returns the data for the given role and section in the header with the specified orientation.
QVariant
LbToolModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();

    // for vertical headers returns row index
    if (orientation == Qt::Vertical) return QVariant(section+1);

    // horizontal
    switch(section) {
    case RunColumn:
        return QVariant(QString("Run Number"));
    case LbColumn:
        return QVariant(QString("LB Number"));
    case FactorColumn:
        return QVariant(QString("F (i)"));
    case OpColumn:
        return QVariant(QString("Operation"));
    case PrevFactorColumn:
        return QVariant(QString("F (i-1)"));
    default:
        return QVariant();
    }
}

// Returns the index of the item in the model specified by the given row, column and parent index.
QModelIndex
LbToolModel::index(int row, int column, const QModelIndex& parent) const
{
    if (parent.isValid()) return QModelIndex();

    return createIndex(row, column);
}

// inserts count rows into the model before the given row
bool
LbToolModel::insertRows(int row, int count, const QModelIndex& parent)
{
    // parent must be root
    if (parent.isValid()) return false;

    // check arguments
    if (row > int(m_data.size())) return false;

    beginInsertRows(parent, row, row+count-1);

    // insert copies data from next row unless inserting after last row
    LbToolEntry newEntry;
    if (m_data.empty()) {
        // get default entry values from config
        Config config;
        double f = config.value("lb_tool/def_factor").toDouble();
        double fprev = config.value("lb_tool/def_prev_factor").toDouble();
        QString strop = config.value("lb_tool/def_op").toString();

        // insert copies data from next row unless inserting after last row
        newEntry = LbToolEntry(0, 0, f, LbToolEntry::Minus, fprev);
        newEntry.setOp(strop);
    } else {
        newEntry = row == int(m_data.size()) ? m_data.back() : m_data[row];
    }
    m_data.insert(m_data.begin()+row, count, newEntry);

    endInsertRows();

    return true;
}

// Returns the parent of the model item with the given index.
QModelIndex
LbToolModel::parent(const QModelIndex&) const
{
    return QModelIndex();
}

// removes count rows starting with the given row under parent parent from the model
bool
LbToolModel::removeRows(int row, int count, const QModelIndex& parent)
{
    // parent must be root
    if (parent.isValid()) return false;

    // check arguments
    if (row >= int(m_data.size()) or row+count > int(m_data.size())) return false;

    beginRemoveRows(parent, row, row+count-1);

    std::vector<LbToolEntry>::iterator start = m_data.begin() + row;
    m_data.erase(start, start + count);

    endRemoveRows();

    return true;
}

// Returns the number of rows under the given parent
int
LbToolModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) return 0;
    return m_data.size();
}

// Sets the role data for the item at index to value
bool
LbToolModel::setData(const QModelIndex & index, const QVariant& value, int role)
{
    if (not index.isValid() or index.row() >= int(m_data.size()) or role != Qt::EditRole) {
        return false;
    }

    LbToolEntry& entry = m_data[index.row()];
    if (index.column() == RunColumn) {
        bool ok;
        uint32_t run = value.toUInt(&ok);
        if (not ok) return false;
        entry.setRun(run);
    } else if (index.column() == LbColumn) {
        if (value.toInt() == -1) {
            entry.setLb(daq::mda::LB::LastLB);
        } else {
            entry.setLb(value.toInt());
        }
    } else if (index.column() == FactorColumn) {
        bool ok;
        double f = value.toDouble(&ok);
        if (not ok) return false;
        entry.setFactor(f);
    } else if (index.column() == OpColumn) {
        if (index.row() == 0) return false;
        if (not entry.setOp(value.toString())) return false;
    } else if (index.column() == PrevFactorColumn) {
        if (index.row() == 0) return false;
        bool ok;
        double f = value.toDouble(&ok);
        if (not ok) return false;
        entry.setPrevFactor(f);
    } else {
        return false;
    }

    emit dataChanged(index, index);
    return true;

}

// move row to a new position
bool
LbToolModel::moveRow(int row, int newRow)
{
    if (not beginMoveRows(QModelIndex(), row, row, QModelIndex(), newRow > row ? newRow+1 : newRow)) {
        return false;
    }

    m_data.insert(m_data.begin() + newRow, m_data[row]);
    // old location may have shifted
    if (newRow < row) ++ row;
    m_data.erase(m_data.begin() + row);

    endMoveRows();

    return true;
}

// Replace current data
void 
LbToolModel::replaceData(const std::vector<LbToolEntry>& entries)
{
    m_initial = entries;
    resetToInitial();
}

// reset model to its initial state
void
LbToolModel::resetToInitial()
{
    beginResetModel();
    m_data = m_initial;
    endResetModel();
}

// copy all factors from given entry to all other entries
void
LbToolModel::copyFactors(int row)
{
    const LbToolEntry& e = m_data[row];
    for (std::vector<LbToolEntry>::iterator it = m_data.begin(); it != m_data.end(); ++ it) {
        it->setFactor(e.factor());
        it->setPrevFactor(e.prevFactor());
        it->setOp(e.op());
    }

    emit dataChanged(index(0, FactorColumn), index(m_data.size()-1, PrevFactorColumn));
}


} // namespace mda_browser
