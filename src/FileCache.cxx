//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/FileCache.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
FileCache::FileCache (unsigned maxSize)
    : m_maxSize(maxSize)
    , m_name2file()
    , m_mru()
{
}

//--------------
// Destructor --
//--------------
FileCache::~FileCache ()
{
}

/// REturn cached or newly open file for given object.
boost::shared_ptr<TFile> 
FileCache::getFile(const std::string& filename, const std::string& dataset,
        const boost::shared_ptr<daq::mda::FileRead>& mdaFileRead)
{
    DSFileName fname(dataset, filename);
    
    // search cache
    CacheMap::const_iterator it = m_name2file.find(fname);
    if (it != m_name2file.end()) {
        // re-order mru list
        m_mru.remove(fname);
        m_mru.push_front(fname);
        // return file pointer
        return it->second;
    }
    
    // Need to open a file
    ERS_DEBUG(0, "FileCache::getFile: open new file " << filename );
    boost::shared_ptr<TFile> file(mdaFileRead->openFile(filename, dataset));

    // cache it
    m_name2file.insert(CacheMap::value_type(fname, file));
    
    // add to mru list
    m_mru.push_front(fname);
    
    // remove old stuff if needed
    while (m_mru.size() > m_maxSize) {
        m_name2file.erase(m_mru.back());
        m_mru.pop_back();
    }

    return file;
}

} // namespace mda_browser
