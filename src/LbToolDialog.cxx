//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/LbToolDialog.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QItemDelegate>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <TH1.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/mda.h"
#include "mda/common/LB.h"
#include "mda_browser/Config.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using boost::property_tree::ptree;
using boost::property_tree::json_parser::read_json;
using boost::property_tree::json_parser::write_json;

namespace {

// few adjusments to the table view editor are needed
class ItemDelegate : public QItemDelegate {
public:
    ItemDelegate(QObject* parent = 0) : QItemDelegate(parent) {}

    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
};

QWidget* 
ItemDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QWidget* editor = QItemDelegate::createEditor(parent, option, index);
    switch (index.column()) {
    case 1:
        if (QSpinBox* spinbox = dynamic_cast<QSpinBox*>(editor)) {
            spinbox->setMinimum(-1);
            spinbox->setMaximum(100000);
            spinbox->setSpecialValueText("EoR");
        }
        break;
    case 2:
        if (QDoubleSpinBox* spinbox = dynamic_cast<QDoubleSpinBox*>(editor)) {
            spinbox->setMinimum(0.0);
            spinbox->setMaximum(1000.0);
            spinbox->setSpecialValueText("Auto");
            spinbox->setSingleStep(0.25);
        }
        break;
    case 4:
        if (QDoubleSpinBox* spinbox = dynamic_cast<QDoubleSpinBox*>(editor)) {
            spinbox->setMinimum(0.0);
            spinbox->setMaximum(1000.0);
            spinbox->setSingleStep(0.25);
        }
        break;
    }
    return editor;
}

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
LbToolDialog::LbToolDialog(QWidget* parent,
        DbConnections* dbconn,
        FileCache* fileCache,
        const QString& partition,
        const QString& server,
        const QString& provider,
        const QString& path,
        const std::vector<LbToolEntry>& entries)
    : QDialog(parent)
    , m_dbconn(dbconn)
    , m_fileCache(fileCache)
    , m_canvasMgr(0)
    , m_model(new LbToolModel(this, entries))
    , m_saveButton(0)
    , m_loadButton(0)
    , m_plotButton(0)
    , m_recentButton(0)
    , m_recentMenu(0)
{
    init();

    // set names
    m_partitionName->setText(partition);
    m_serverName->setText(server);
    m_providerName->setText(provider);
    m_histName->setText(path);
}

void
LbToolDialog::init()
{
    ERS_DEBUG(0, "LbToolDialog: init @" << this);

    setupUi(this);

    // don't wait for this window to close application
    setAttribute(Qt::WA_DeleteOnClose, true);
    setAttribute(Qt::WA_QuitOnClose, false);

    // instantiate canvas manager 
    m_canvasMgr = new CanvasMgr(this);

    // add few buttons
    m_plotButton = buttonBox->addButton("Plot", QDialogButtonBox::ApplyRole);
    m_plotButton->setDefault(true);
    m_plotButton->setShortcut(QKeySequence::Print);
    m_saveButton = buttonBox->addButton("Save", QDialogButtonBox::ResetRole);
    m_saveButton->setShortcut(QKeySequence::Save);
    m_loadButton = buttonBox->addButton("Load", QDialogButtonBox::ResetRole);
    m_loadButton->setShortcut(QKeySequence::Open);
    m_recentButton = buttonBox->addButton("Recent", QDialogButtonBox::ResetRole);
    m_recentMenu = new QMenu(this);
    m_recentButton->setMenu(m_recentMenu);

    fillRecentMenu();
    
    // the code in this class assumes single whole-row selection, in principle
    // this should be set in setupUi(), but in case it did not happen we do it here
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    tableView->setModel(m_model);
    tableView->setItemDelegate(new ItemDelegate(this));

    connect(tableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
            SLOT(updateButtons()));
    connect(addRowButton, SIGNAL(clicked()), SLOT(insertRow()));
    connect(removeRowsButton, SIGNAL(clicked()), SLOT(removeRow()));

    connect(resetButton, SIGNAL(clicked()), m_model, SLOT(resetToInitial()));
    connect(resetButton, SIGNAL(clicked()), SLOT(updateButtons()));
    connect(moveRowsUpButton, SIGNAL(clicked()), SLOT(moveRowUp()));
    connect(moveRowsDownButton, SIGNAL(clicked()), SLOT(moveRowDown()));
    connect(applyButton, SIGNAL(clicked()), SLOT(applyFactors()));

    connect(m_saveButton, SIGNAL(clicked()), SLOT(saveData()));
    connect(m_loadButton, SIGNAL(clicked()), SLOT(loadData()));
    connect(m_recentMenu, SIGNAL(triggered(QAction*)), SLOT(loadRecent(QAction*)));
    connect(m_plotButton, SIGNAL(clicked()), SLOT(plot()));

    updateButtons();
}

//--------------
// Destructor --
//--------------
LbToolDialog::~LbToolDialog()
{
    ERS_DEBUG(0, "LbToolDialog: destructor @" << this);
}

// returns the index of the selected row, returns -1 if no selection
int
LbToolDialog::selectedRow() const
{
    const QModelIndexList& select = tableView->selectionModel()->selectedRows();

    // single-row selection is expected
    if (select.isEmpty()) return -1;
    return select.front().row();
}

// Update file names and histograms paths of entries that miss this info
bool
LbToolDialog::updateFileNames()
{
    std::vector<LbToolEntry>& entries = m_model->entries();

    std::map<uint32_t, std::set<daq::mda::HistoFile> > run2hfiles;
    for (std::vector<LbToolEntry>::iterator it = entries.begin(); it != entries.end(); ++ it) {
        if (it->fileName().isEmpty() or it->histoPath().isEmpty()) {

            uint32_t run = it->run();

            if (run2hfiles.find(run) == run2hfiles.end()) {
                // read data for this run from database
                boost::shared_ptr<daq::mda::DBRead> db = m_dbconn->getMda();
                if (not db) return false;

                db->histogram(run2hfiles[run], qPrintable(m_partitionName->text()), run,
                        qPrintable(m_serverName->text()), qPrintable(m_providerName->text()),
                        qPrintable(m_histName->text()));
            }

            const std::set<daq::mda::HistoFile>& hfiles = run2hfiles[run];
            bool found = false;
            for (std::set<daq::mda::HistoFile>::const_iterator hit = hfiles.begin(); hit != hfiles.end(); ++ hit) {
                if (hit->lb() == it->lb()) {
                    it->setFileName(hit->file().c_str());
                    it->setDataset(hit->dataset().c_str());
                    it->setHistoPath(hit->histoPath().c_str());
                    found = true;
                    break;
                }
            }
            if (not found) {
                QString msg("Failed to find histogram %1\nfor run %2 and LB %3");
                QMessageBox::critical(this, "Histogram not found", msg.arg(m_histName->text()).arg(run).arg(it->lb()));
                return false;
            }

        }
    }

    return true;
}

// update buttons based on the model state and selection
void
LbToolDialog::updateButtons()
{
    Config config;
    int max_entries = config.value("lb_tool/max_entries").toInt();

    int row = selectedRow();

    addRowButton->setEnabled(m_model->rowCount() < max_entries);
    removeRowsButton->setEnabled(row >= 0);
    moveRowsUpButton->setEnabled(row > 0);
    moveRowsDownButton->setEnabled(row >= 0 and row+1 < m_model->rowCount());
    applyButton->setEnabled(row >= 0);
    
    m_plotButton->setEnabled(m_model->rowCount() > 1);
}

// insert row before selected or after last one if no selection
void
LbToolDialog::insertRow()
{
    int row = selectedRow();
    if (row < 0) row = m_model->rowCount();
    if (m_model->insertRows(row, 1, QModelIndex())) {
        // select it so that it's clearly identifiable
        tableView->selectRow(row);
    }
    updateButtons();
}

// remove selected rows, rows must be fully selected
void
LbToolDialog::removeRow()
{
    int row = selectedRow();
    if (row < 0) return;

    m_model->removeRows(row, 1, QModelIndex());

    updateButtons();
}

// move selected row up
void
LbToolDialog::moveRowUp()
{
    int row = selectedRow();
    if (row <= 0) return;

    if (m_model->moveRow(row, row-1)) {
        tableView->selectRow(row-1);
        updateButtons();
    }
}

// move selected row down
void
LbToolDialog::moveRowDown()
{
    int row = selectedRow();
    if (row < 0) return;
    if (row+1 >= m_model->rowCount()) return;

    // moveRow accepts indices in current mode, so it's off by one
    if (m_model->moveRow(row, row+2)) {
        tableView->selectRow(row+1);
        updateButtons();
    }
}

void 
LbToolDialog::addRecent(const QString& str)
{
    Config config;
    
    // get old mru lust
    QStringList mru = config.value("lb_tool/mru").toStringList();
    
    // add entry, remove same name if there already
    mru.insert(0, str);
    mru.removeDuplicates();

    // limit list size
    int mru_size = config.value("lb_tool/mru_size").toInt(); 
    while (mru.size() > 0 and mru.size() > mru_size) {
        mru.removeLast();
    }

    // store it back
    config.setValue("lb_tool/mru", mru);

    // update menu
    fillRecentMenu();
}

void 
LbToolDialog::removeRecent(const QString& str)
{
    Config config;
    
    // get old mru lust
    QStringList mru = config.value("lb_tool/mru").toStringList();
    
    // remove a name if there already
    mru.removeAll(str);

    // store it back
    config.setValue("lb_tool/mru", mru);

    // update menu
    fillRecentMenu();
}

void 
LbToolDialog::fillRecentMenu()
{
    m_recentMenu->clear();
    
    Config config;
    QStringList mru = config.value("lb_tool/mru").toStringList();
    for (QStringList::const_iterator it = mru.begin(); it != mru.end(); ++ it) {
        // make menu entry from full path name
        // for now use full path
        QAction* action = m_recentMenu->addAction(*it);
        
        // store full path name as user data
        action->setData(*it);
    }

    m_recentButton->setEnabled(not mru.isEmpty());
}

// save data to a file
void 
LbToolDialog::saveData()
{
    Config config;
    const QString& dir = config.value("mda_browser/directory").toString();

    QString fname = QFileDialog::getSaveFileName(this, "Open file", dir, "LB Tool Files (*.lbt);;All files (*)");
    if (fname.isEmpty()) return;
    ERS_DEBUG(0, "LbToolDialog::saveData: dialog returned name " << qPrintable(fname));

    QFileInfo finfo(fname);
    ERS_DEBUG(0, "LbToolDialog::saveData: absoluteFilePath " << qPrintable(finfo.absoluteFilePath()));

    // remember directory
    config.setValue("mda_browser/directory", finfo.absolutePath());

    saveData(finfo.absoluteFilePath());
    
}

// save data to a given file name
void 
LbToolDialog::saveData(const QString& fname)
{
    // write out the data
    try {
        QSettings settings(fname, QSettings::IniFormat);
        settings.beginGroup("lbtool");
        settings.setValue("partition", m_partitionName->text());
        settings.setValue("server", m_serverName->text());
        settings.setValue("provider", m_providerName->text());
        settings.setValue("path", m_histName->text());
        settings.endGroup();

        const std::vector<LbToolEntry>& entries = m_model->entries();
        settings.beginWriteArray("entries", entries.size());
        for (unsigned i = 0; i != entries.size(); ++ i) {
            const LbToolEntry& entry = entries[i];
            settings.setArrayIndex(i);
            settings.setValue("run", entry.run());
            settings.setValue("lb", entry.lb());
            settings.setValue("factor", entry.factor());
            settings.setValue("prevFactor", entry.prevFactor());
            settings.setValue("op", entry.opString());
            settings.setValue("fileName", entry.fileName());
            settings.setValue("dataset", entry.dataset());
            settings.setValue("histoPath", entry.histoPath());
        }
        settings.endArray();

    } catch (const std::exception& ex) {
        QString msg = "Failed to build property tree:\n";
        msg += ex.what();
        QMessageBox::critical(this, "Failed to build property tree", msg);
        return;
    }
    
    // add this file to MRU
    addRecent(fname);
}


// load data from a file 
void 
LbToolDialog::loadData()
{
    Config config;
    const QString& dir = config.value("mda_browser/directory").toString();

    QString fname = QFileDialog::getOpenFileName(this, "Open file", dir, "LB Tool Files (*.lbt);;All files (*)");
    if (fname.isEmpty()) return;
    
    QFileInfo finfo(fname);
    
    // remember directory
    config.setValue("mda_browser/directory", finfo.absolutePath());
    
    // read the data
    loadData(finfo.absoluteFilePath());

    updateButtons();
}

// load data from a file 
void 
LbToolDialog::loadRecent(QAction* action)
{
    // get user data which file name
    const QString& path = action->data().toString();
    if (path.isEmpty()) return;
    
    // check that file exists
    QFileInfo finfo(path);
    if (not finfo.exists()) {
        removeRecent(path);
        QMessageBox::warning(this, "Missing file", "File "+path+" does not exist");
        return;
    }

    // read the data
    loadData(finfo.absoluteFilePath());

    updateButtons();
}

// save data from a given file name
void 
LbToolDialog::loadData(const QString& fname)
{
    try {

        QSettings settings(fname, QSettings::IniFormat);

        // parse the data and replace our state
        settings.beginGroup("lbtool");
        const QString& partition = settings.value("partition").toString();
        const QString& server = settings.value("server").toString();
        const QString& provider = settings.value("provider").toString();
        const QString& path = settings.value("path").toString();
        settings.endGroup();

        std::vector<LbToolEntry> entries;
        int size = settings.beginReadArray("entries");
        for (int i = 0; i != size; ++ i) {

            settings.setArrayIndex(i);
            uint32_t run = settings.value("run").toUInt();
            uint32_t lb = settings.value("lb").toUInt();
            double f = settings.value("factor").toDouble();
            double fprev = settings.value("prevFactor").toDouble();
            const QString& op = settings.value("op").toString();
            const QString& fileName = settings.value("fileName", "").toString();
            const QString& dataset = settings.value("dataset", "").toString();
            const QString& histoPath = settings.value("histoPath", "").toString();

            LbToolEntry entry(run, lb, f, LbToolEntry::Minus, fprev, fileName, dataset, histoPath);
            entry.setOp(op);
            entries.push_back(entry);
        }
        settings.endArray();

        // replace all our data
        m_model->replaceData(entries);
        m_partitionName->setText(partition);
        m_serverName->setText(server);
        m_providerName->setText(provider);
        m_histName->setText(path);
        updateButtons();

    } catch (const std::exception& ex) {
        
        QString msg = "Failure while parsing data:\n";
        msg += ex.what();
        QMessageBox::critical(this, "Failed to load file", msg);
        return;

    }

    // add this file to MRU
    addRecent(fname);
}


// make a plot based on current data 
void 
LbToolDialog::plot()
{
    // check that every entry has file name and histo path
    if (not updateFileNames()) return;

    boost::shared_ptr<daq::mda::FileRead> fileRead = m_dbconn->getFileRead();
    if (not fileRead) {
        // error message should have been displayed already
        return;
    }

    std::vector<LbToolEntry>& entries = m_model->entries();
    std::vector<TH1*> histos;
    histos.reserve(entries.size());
    for (std::vector<LbToolEntry>::iterator it = entries.begin(); it != entries.end(); ++ it) {

        boost::shared_ptr<TFile> tfile = m_fileCache->getFile(qPrintable(it->fileName()), qPrintable(it->dataset()), fileRead);
        if (not tfile) {
            // display error message here
            ERS_LOG("LbToolDialog::plot: failed to open file " << qPrintable(it->fileName()));
            QString msg = "Failed to locate or open file\n%1";
            QMessageBox::critical(this, "Cannot open file", msg.arg(it->fileName()));
            return;
        }
        ERS_DEBUG(0, "LbToolDialog::plot: opened file " << tfile->GetName());

        // read histogram, do not add it to memory directory
        TH1::AddDirectory(kFALSE);
        TH1* hist = static_cast<TH1*>(tfile->Get(qPrintable(it->histoPath())));
        if (not hist) {
            // display error message here
            ERS_LOG("LbToolDialog::plot: failed to locate object " << qPrintable(it->histoPath()));
            QString msg = "Failed to locate histogram\n%1\nin a file %2";
            QMessageBox::critical(this, "Cannot find histogram", msg.arg(it->histoPath(), it->fileName()));
            return;
        }

        histos.push_back(hist);
    }

    
    // apply all operations
    for (int i = int(histos.size()) - 1; i >= 0; -- i) {

        double scale = entries[i].factor();
        double scalePrev = entries[i].prevFactor();

        if (i == 0 or (scale > 0 and scalePrev == 0)) {

            // do not use previous histo, just plot current one with scale factor
            
            // first entry can be scaled
            if (scale == 0) scale = 1.;
            if (scale != 1) histos[i]->Scale(scale);
            
            // update title
            QString title("#splitline{%1}{[%2(run=%3, LB=%4)]}");
            title = title.arg(histos[i]->GetTitle())
                        .arg(scale != 1 ? QString("%1").arg(scale) : QString())
                        .arg(entries[i].run())
                        .arg(daq::mda::LB::lb2string(entries[i].lb()).c_str());
            histos[i]->SetTitle(qPrintable(title));

            // update name
            QString name("%1_r%2_lb%3");
            name = name.arg(histos[i]->GetName())
                        .arg(entries[i].run())
                        .arg(daq::mda::LB::lb2string(entries[i].lb()).c_str());
            histos[i]->SetName(qPrintable(name));

        } else {

            if (scale == 0.) {
                // auto-scale previous histogram
                scale = 1.;
                scalePrev = 1.;
                double entries = histos[i]->GetEntries();
                double entriesPrev = histos[i-1]->GetEntries();
                ERS_DEBUG(0, "LbToolDialog::plot: entries=" << entries << " entriesPrev=" << entriesPrev);
                if (entriesPrev > 0) {
                    scalePrev = entries / entriesPrev;
                }
            }
            
            // update title
            QString title("#splitline{%1}{[%2(run=%3, LB=%4) %5 %6(run=%7, LB=%8)]}");
            title = title.arg(histos[i]->GetTitle())
                        .arg(scale != 1 ? QString("%1").arg(scale) : QString())
                        .arg(entries[i].run())
                        .arg(daq::mda::LB::lb2string(entries[i].lb()).c_str())
                        .arg(entries[i].opString())
                        .arg(scalePrev != 1 ? QString("%1").arg(scalePrev) : QString())
                        .arg(entries[i-1].run())
                        .arg(daq::mda::LB::lb2string(entries[i-1].lb()).c_str());
            histos[i]->SetTitle(qPrintable(title));

            // update name
            QString name("%1_r%2_lb%3_op_r%4_lb%5");
            name = name.arg(histos[i]->GetName())
                        .arg(entries[i].run())
                        .arg(daq::mda::LB::lb2string(entries[i].lb()).c_str())
                        .arg(entries[i-1].run())
                        .arg(daq::mda::LB::lb2string(entries[i-1].lb()).c_str());
            histos[i]->SetName(qPrintable(name));

            switch (entries[i].op()) {
            case LbToolEntry::Minus:
                histos[i]->Add(histos[i], histos[i-1], scale, -scalePrev);
                break;
            case LbToolEntry::Plus:
                histos[i]->Add(histos[i], histos[i-1], scale, scalePrev);
                break;
            case LbToolEntry::Multiply:
                histos[i]->Multiply(histos[i], histos[i-1], scale, scalePrev);
                break;
            case LbToolEntry::Divide:
                histos[i]->Divide(histos[i], histos[i-1], scale, scalePrev);
                break;
            }
            
        }
    }

    
    // make a new canvas if needed
    CanvasWindow* canvas = m_canvasMgr->lastCanvas();

    // clear it
    canvas->clear();

    // add all histograms
    for (std::vector<TH1*>::iterator it = histos.begin(); it != histos.end(); ++ it) {
        canvas->add(*it);
    }
    canvas->show();

}


// copy factors and op from selected row to all other rows
void
LbToolDialog::applyFactors()
{
    int row = selectedRow();
    if (row < 0) return;

    m_model->copyFactors(row);
}


} // namespace mda_browser
