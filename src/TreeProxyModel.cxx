//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/TreeProxyModel.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNode.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
TreeProxyModel::TreeProxyModel(QObject* parent)
  : QSortFilterProxyModel(parent)
{
}

//--------------
// Destructor --
//--------------
TreeProxyModel::~TreeProxyModel ()
{
}

/// Returns true if the item in the column indicated by the given source_column and source_parent
/// should be included in the model
bool
TreeProxyModel::filterAcceptsColumn(int source_column, const QModelIndex&) const
{
    bool res = source_column == filterKeyColumn();
    return res;
}

/// Returns true if the item in the row indicated by the given source_row and source_parent
/// should be included in the model
bool
TreeProxyModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    const ModelNode* parent = static_cast<const ModelNode*>(source_parent.internalPointer());

    bool res = true;
    if (parent) {
        // exclude all leaf nodes
        if (const ModelNode* child = parent->child(source_row)) {
            res = not child->leaf();
        }
    }
    return res;
}

} // namespace mda_browser
