//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/CanvasMgr.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
CanvasMgr::CanvasMgr (QWidget* parent)
    : QObject(parent)
    , m_parent(parent)
{
}

//--------------
// Destructor --
//--------------
CanvasMgr::~CanvasMgr ()
{
}

// make new canvas window
CanvasWindow* 
CanvasMgr::newCanvas()
{
    CanvasWindow* canvas = new CanvasWindow(m_parent);
    return canvas;
}

// get last or make new canvas window
CanvasWindow* 
CanvasMgr::lastCanvas()
{
    // find last child with the type CanvasWindow
    CanvasWindow* canvas = 0;
    const QObjectList& children = m_parent->children();
    for (int i = children.size(); i > 0; -- i) {
        if (CanvasWindow* c = dynamic_cast<CanvasWindow*>(children[i-1])) {
            canvas = c;
            break;
        }
    }
    
    // make a new one if not found
    if (not canvas) canvas = newCanvas();
    
    return canvas;
}

} // namespace mda_browser
