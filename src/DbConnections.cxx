//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/DbConnections.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include <QMessageBox>
#include "mda_browser/Config.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
DbConnections::DbConnections (QObject* parent)
    : QObject(parent)
    , m_mdaDb()
    , m_mdaFileRead()
{
}

//--------------
// Destructor --
//--------------
DbConnections::~DbConnections ()
{
}

// Returns MDA client object. If connection cannot be established
// it will show warning message box and return zero pointer.
boost::shared_ptr<daq::mda::DBRead> 
DbConnections::getMda()
{
    if (not m_mdaDb) {
        
        Config config;
        
        QVariant val = config.value("database/mda_conn_str");
        QString connStr;
        if (not val.isNull()) connStr = val.toString();
        
        if (connStr.isEmpty()) {
            m_mdaDb.reset(new daq::mda::DBRead());
        } else {
            m_mdaDb.reset(new daq::mda::DBRead(connStr.toLatin1().constData()));
        }
        
    }
    
    return m_mdaDb;
}

// Returns file reader client. If connection cannot be established
// it will show warning message box and return zero pointer.
boost::shared_ptr<daq::mda::FileRead> 
DbConnections::getFileRead()
{
    if (not m_mdaFileRead) {
        
        Config config;
        
        QVariant val = config.value("database/coca_conn_str");
        QString connStr;
        if (not val.isNull()) connStr = val.toString();
        
        if (connStr.isEmpty()) {
            m_mdaFileRead.reset(new daq::mda::FileRead());
        } else {
            m_mdaFileRead.reset(new daq::mda::FileRead(connStr.toLatin1().constData()));
        }
    }
    
    return m_mdaFileRead;
}

// reset connections if config changed
void 
DbConnections::reset()
{
    m_mdaDb.reset();
    m_mdaFileRead.reset();
}

} // namespace mda_browser
