//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/Config.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
Config::Config ()
    : m_config()
{
}

//--------------
// Destructor --
//--------------
Config::~Config ()
{
}

// Returns the value for setting key. If the setting doesn't exist, returns defaultValue.
// If defaultValue is not specified returns application default.
QVariant
Config::value(const QString& key, const QVariant& defVal) const
{
    auto value = m_config.value(key, defVal.isNull() ? defaultValue(key) : defVal);

    // Protection against default connection strings which are not valid anymore.
    // Replace those with empty/null strings which now indicate "default".
    if (value.isValid()) {
        if (key == "database/mda_conn_str" and value.toString() == "oracle://ATLAS_MDA/ATLAS_MDA") {
            value.clear();
            m_config.remove(key);
        } else if (key == "database/coca_conn_str" and value.toString() == "oracle://ATLAS_COCA/ATLAS_COCA") {
            value.clear();
            m_config.remove(key);
        }
    }
    return value;
}

// Sets the value of setting key to value. If the key already exists, the previous value is overwritten.
void
Config::setValue(const QString& key, const QVariant& value)
{
    // remove null value, keep non-null
    if (value.isNull()) {
        m_config.remove(key);
    } else {
        m_config.setValue(key, value);
    }
}


// Returns application default value for setting key.
QVariant
Config::defaultValue(const QString& key) const
{
    if (key == "database/mda_conn_str") return QVariant();
    if (key == "database/coca_conn_str") return QVariant();
    if (key == "database/number_of_runs") return QVariant(1000);
    if (key == "lb_tool/mru_size") return QVariant(8);
    if (key == "lb_tool/max_entries") return QVariant(30);
    if (key == "lb_tool/def_factor") return QVariant(1.0);
    if (key == "lb_tool/def_prev_factor") return QVariant(1.0);
    if (key == "lb_tool/def_op") return QVariant("-");
    return QVariant();
}

} // namespace mda_browser
