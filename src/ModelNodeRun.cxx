//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeRun.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>
#include <QDateTime>
#include <QMessageBox>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNodePartition.h"
#include "mda_browser/ModelNodeProvider.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeRun::ModelNodeRun (const daq::mda::RunInfo& runInfo)
    : ModelNode()
    , m_runInfo(runInfo)
{
}

//--------------
// Destructor --
//--------------
ModelNodeRun::~ModelNodeRun ()
{
}

/// get partition name
QString
ModelNodeRun::partition() const
{
    ModelNodePartition* part = static_cast<ModelNodePartition*>(parent());
    return part->partition();
}

/// Returns number of columns for children
int
ModelNodeRun::columnCount() const
{
    return ModelNodeProvider::classColumnCount();
}

/// Returns column header for children
QString
ModelNodeRun::columnHeader(int column) const
{
    return ModelNodeProvider::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodeRun::columnValue(int column) const
{
    switch(column) {
    case 0:
        return QVariant(m_runInfo.run());
    case 1:
        return QVariant(m_runInfo.runType().c_str());
    case 2: {
        QDateTime dt;
        dt.setTime_t(m_runInfo.time());
        return QVariant(dt);
    }
    default:
        return QVariant();
    }
}

/// Return icon for this object
const QIcon&
ModelNodeRun::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/record_16x16.png");
        icon.addFile(":/Icons/images/record_32x32.png");
    }
    return icon;
}


/// Load or refresh children data from database
void
ModelNodeRun::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
        std::vector<boost::shared_ptr<ModelNode> >& children)
{
    ModelNodePartition* part = static_cast<ModelNodePartition*>(parent());

    ERS_DEBUG(0, "ModelNodeRun::populate: run: " << m_runInfo.run());

    if (not mdaDb) return;

    std::set<std::string> ohs_servers;

    // get the list of servers for this run
    try {
        mdaDb->ohsServers(ohs_servers, qPrintable(part->partition()), m_runInfo.run());
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        QString msg = "Failed to retrieve OH servers information from database:\n%1";
        QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
        return;
    }

    std::map<std::string, std::string> all_ohs_providers;
    for (std::set<std::string>::const_iterator it = ohs_servers.begin(); it != ohs_servers.end();
            ++it) {

        // get list of providers for this server
        std::set < std::string > ohs_providers;
        try {
            mdaDb->ohsProviders(ohs_providers, qPrintable(part->partition()), m_runInfo.run(), *it);
        } catch (const ers::Issue& ex) {
            ers::error(ex);
            QString msg = "Failed to retrieve OH providers information from database:\n%1";
            QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
            return;
        }

        // store them in common map
        for (std::set<std::string>::const_iterator pit = ohs_providers.begin();
                pit != ohs_providers.end(); ++pit) {
            all_ohs_providers.insert(std::make_pair(*pit, *it));
        }
    }

    // add all map entries to a list
    children.reserve(all_ohs_providers.size());
    typedef std::map<std::string, std::string>::const_iterator Iter;
    for (Iter it = all_ohs_providers.begin(); it != all_ohs_providers.end(); ++ it) {
        children.push_back(boost::make_shared<ModelNodeProvider>(it->first.c_str(), it->second.c_str()));
    }
}

// column header for this class
QString
ModelNodeRun::classColumnHeader(int column)
{
    static const char* headers[] = { "Run Number", "Run Type", "Registration Time" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
