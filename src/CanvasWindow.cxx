//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/CanvasWindow.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cmath>
#include <TFile.h>
#include <TH1.h>
#include <TList.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TSystem.h>
#include <QCloseEvent>
#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <QImage>
#include <QPainter>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/Config.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
CanvasWindow::CanvasWindow (QWidget* parent)
    : QMainWindow(parent)
    , Ui::CanvasWindow()
    , m_canvas(0)
{
    setupUi(this);
    
    // don't wait for this window to close application
    setAttribute(Qt::WA_DeleteOnClose, true);

    m_canvas = m_canvasWidget->canvas();

    connect(m_actionClear, SIGNAL(triggered()), m_canvasWidget, SLOT(clear()));
    connect(m_actionSave, SIGNAL(triggered()), SLOT(saveCanvas()));

    restoreGeometry();
}

//--------------
// Destructor --
//--------------
CanvasWindow::~CanvasWindow ()
{
}


// Delete all contents from a canvas
void 
CanvasWindow::clear()
{
    m_canvasWidget->clear();
}

// Add one more object and show it in a new pad
void 
CanvasWindow::add(TObject* obj)
{
//    qDebug() << "CanvasWindow: add() mapped = " << int(m_centralWidget->testAttribute(Qt::WA_Mapped));

    // make new pad
    TPad* pad = new TPad( "", "", 0., 0., 1., 1. );
    pad->SetBit(kCanDelete);

    // add object to pad
    pad->GetListOfPrimitives()->Add(obj);
    pad->Modified(kTRUE);
    pad->Update();

    // draw pad in a canvas, this will make canvas a parent of pad
    m_canvas->cd();
    pad->Draw();

    // re-flow the pads if more than one pad
    TIter iter(m_canvas->GetListOfPrimitives());
    int nPads = 0;
    while (TObject* obj = iter()) {
        if (obj->InheritsFrom(TPad::Class())) ++nPads;
    }

    if (nPads > 1) {

        // calculate number of columns/rows
        int ncol = (int)std::sqrt(nPads);
        if (nPads > ncol*ncol) ncol ++;
        int nrow = (nPads + ncol - 1) / ncol;

        // pad width/height
        float height = 1./(float)(nrow);
        float width = 1./(float)(ncol);

        // resize and move each pad
        iter.Reset();
        int pad = 0;
        while (TObject* obj = iter()) {
            if (obj->InheritsFrom(TPad::Class())) {
                int row = pad / ncol;
                int col = pad % ncol;
                TPad* tpad = static_cast<TPad*>(obj);
                tpad->SetPad( col*width, 1. - (row+1)*height, (col + 1)*width, 1. - row*height );
                ++ pad;
            }
        }
    }

    m_canvas->Modified(kTRUE);
    m_canvas->Update();
//    m_centralWidget->Refresh();
}

void 
CanvasWindow::closeEvent(QCloseEvent *event)
{
    saveGeometry();
    QMainWindow::closeEvent(event);
}

/// save main window geometry
void 
CanvasWindow::saveGeometry()
{
    Config config;
  
    config.setValue("geometry/canvas_window", QMainWindow::saveGeometry());
}

/// restore main window geometry
void 
CanvasWindow::restoreGeometry()
{
    Config config;

    QMainWindow::restoreGeometry(config.value("geometry/canvas_window").toByteArray());
}

/// save canvas image as PNG
void 
CanvasWindow::saveCanvas()
{
    Config config;
    QString dir = config.value("mda_browser/directory").toString();

    ERS_DEBUG(0, "CanvasWindow::save: dir = " << qPrintable(dir));

    // open file name dialog
    QString filter ="PNG image (*.png *.PNG);;PostScript file (*.ps *.eps);;PDF file (*.pdf);;ROOT file (*.root);;ROOT C++ macro (*.C);;XML file (*.xml)";
    QString selectedFilter = config.value("mda_browser/canvas_file_type").toString();
    QString fname = QFileDialog::getSaveFileName(this, "File for canvas contents", dir, filter, &selectedFilter);
    
    if (fname.isEmpty()) return;
    
    // get file's directory
    QFileInfo finfo(fname);
    dir = finfo.absolutePath();
    ERS_DEBUG(0, "CanvasWindow::save: fname = " << qPrintable(fname) << " dir = " << qPrintable(dir) 
            << " selectedFilter = " << qPrintable(selectedFilter));

    // save directory for next time
    config.setValue("mda_browser/directory", dir);
    config.setValue("mda_browser/canvas_file_type", selectedFilter);
    
    if (selectedFilter.startsWith("ROOT file")) {

        if (not fname.endsWith(".root")) fname += ".root";

        // m_canvas->SaveAs() can write ROOT files but it only stores canvas object,
        // here we store individual histograms instead of whole canvas
        
        // open file
        TFile f(qPrintable(fname), "RECREATE");

        // iterate over sub-pads
        TIter iter(m_canvas->GetListOfPrimitives());
        while (TObject* obj = iter()) {
            if (TPad* pad = dynamic_cast<TPad*>(obj)) {
                // iterator over histograms in a subpad (there should be just one)
                TIter hiter(pad->GetListOfPrimitives());
                while (TObject* padobj = hiter()) {
                    if (TH1* hist = dynamic_cast<TH1*>(padobj)) {
                        f.WriteTObject(hist);
                    }
                }
            }
        }

        f.Close();
        
    } else {
        
        // if file does not have correct extension SaveAs may be confused
        if (selectedFilter.startsWith("PNG image")) {
            if (not (fname.endsWith(".png") or fname.endsWith(".PNG"))) fname += ".png";
        } else if (selectedFilter.startsWith("PostScript file")) {
            if (not (fname.endsWith(".ps") or fname.endsWith(".eps"))) fname += ".eps";
        } else if (selectedFilter.startsWith("ROOT C++ macro")) {
            if (not fname.endsWith(".C")) fname += ".C";
        } else if (selectedFilter.startsWith("PDF file")) {
            if (not fname.endsWith(".pdf")) fname += ".pdf";
        } else if (selectedFilter.startsWith("XML file")) {
            if (not fname.endsWith(".xml")) fname += ".xml";
        }

        // this function will figure out output format based on file extension
        m_canvas->SaveAs(qPrintable(fname));
    }
}

} // namespace mda_browser
