//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda_browser/ModelNodeFolder.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/make_shared.hpp>
#include <QMessageBox>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda_browser/ModelNodeHistogram.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace mda_browser {

//----------------
// Constructors --
//----------------
ModelNodeFolder::ModelNodeFolder (const QString& name)
    : ModelNode()
    , m_name(name)
{
}

//--------------
// Destructor --
//--------------
ModelNodeFolder::~ModelNodeFolder ()
{
}

/// get absolute folder name, empty string for top-level folder
QString
ModelNodeFolder::path() const
{
    if (m_name.isEmpty()) return m_name;
    return static_cast<ModelNodeFolder*>(parent())->path() + "/" + m_name;
}

/// get partition name
QString
ModelNodeFolder::partition() const
{
    return static_cast<ModelNodeFolder*>(parent())->partition();
}

/// get run number
unsigned
ModelNodeFolder::run() const
{
    return static_cast<ModelNodeFolder*>(parent())->run();
}

/// get server name
QString
ModelNodeFolder::server() const
{
    return static_cast<ModelNodeFolder*>(parent())->server();
}

/// get provider name
QString
ModelNodeFolder::provider() const
{
    return static_cast<ModelNodeFolder*>(parent())->provider();
}

/// Returns number of columns for children
int
ModelNodeFolder::columnCount() const
{
    return ModelNodeHistogram::classColumnCount();
}

/// Returns column header for children
QString
ModelNodeFolder::columnHeader(int column) const
{
    return ModelNodeHistogram::classColumnHeader(column);
}

/// Return data for a given column
QVariant
ModelNodeFolder::columnValue(int column) const
{
    switch(column) {
    case 0:
        return QVariant(m_name);
    default:
        return QVariant();
    }
}

/// Return icon for this object
const QIcon&
ModelNodeFolder::icon() const
{
    static QIcon icon;
    if (icon.isNull()) {
        icon.addFile(":/Icons/images/folder_16x16.png");
        icon.addFile(":/Icons/images/folder_32x32.png");
    }
    return icon;
}


/// Load or refresh children data from database
void
ModelNodeFolder::fetchChildren(const boost::shared_ptr<daq::mda::DBRead>& mdaDb,
        std::vector<boost::shared_ptr<ModelNode> >& children)
{
    ERS_DEBUG(0, "ModelNodeFolder::populate: folder: " << qPrintable(m_name));

    if (not mdaDb) return;

    std::set < std::string > folders;
    std::set < std::string > hnames;

    try {
        mdaDb->listdir(folders, hnames, qPrintable(partition()), run(), 
                qPrintable(server()), qPrintable(provider()), qPrintable(path()));
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        QString msg = "Failed to retrieve folder information from database:\n%1";
        QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
        return;
    }

    // add folders as children
    children.reserve(folders.size() + hnames.size());
    for (std::set<std::string>::const_iterator it = folders.begin(); it != folders.end(); ++it) {
        children.push_back(boost::make_shared<ModelNodeFolder>(it->c_str()));
    }

    if (not hnames.empty()) {
        // get histogram info and add as a child too
        std::set<daq::mda::HistoFile> histos;
        try {
            mdaDb->histograms(histos, qPrintable(partition()), run(), qPrintable(server()),
                qPrintable(provider()), qPrintable(path()));
        } catch (const ers::Issue& ex) {
            ers::error(ex);
            QString msg = "Failed to retrieve histogram information from database:\n%1";
            QMessageBox::critical(0, "Database query error", msg.arg(ex.what()));
            return;
        }

        // group by histo name
        std::map<std::string, std::set<daq::mda::HistoFile>> name2histos;
        for (const auto& histo: histos) {
            name2histos[histo.histoName()].insert(histo);
        }

        // add all histos as children
        for (const auto& pair: name2histos) {
            // use basename
            std::string hname = pair.first;
            std::string::size_type p = hname.rfind('/');
            if (p != std::string::npos) {
                hname.erase(0, p+1);
            }
            children.push_back(boost::make_shared<ModelNodeHistogram>(hname.c_str(), pair.second));
        }
    }
}

// column header for this class
QString
ModelNodeFolder::classColumnHeader(int column)
{
    static const char* headers[] = { "Name" };

    if (column >= 0 and column < int(sizeof headers/sizeof headers[0])) return headers[column];
    return QString();
}

} // namespace mda_browser
